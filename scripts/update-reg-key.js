
const configFile = './config.yml';

const fs = require( 'fs' )
	, process = require( 'process' )
	, path = require( 'path' )
	, yaml = require( 'js-yaml' )
	, lodash = require( 'lodash' )
	, Bluebird = require( 'bluebird' )
	;

if ( process.argv.length < 3 ) {
	console.error( `Usage: node ${ path.basename( __filename ) } <apikey>` );
	process.exit( 1 );
}

let apiKey = process.argv[ 2 ];

Bluebird.resolve( configFile )
	.then( lodash.partial( fs.readFileSync, lodash, 'UTF-8' ) )
	.then( yaml.safeLoad )
	.then( yamlcontent => { yamlcontent.rest.key = apiKey; return yamlcontent; } )
	.then( yaml.dump )
	.then( lodash.partial( fs.writeFileSync, configFile, lodash, 'UTF-8' ) )
	.tap( () => console.info( 'API Key updated succesfully.' ) );