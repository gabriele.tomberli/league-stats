
const lodash = require('lodash');
const monk = require('monk');

let content = require('fs').readFileSync('champions.json', 'UTF-8');
let data = JSON.parse(content);

let champions = lodash(data.data).map((v,k) => { return { _id: parseFloat( v.key ), name: v.name, title: v.title } }).value();
let db = monk('mongodb://localhost:27017/league-stats');
let collection = db.create( 'anag_champions', null, { castIds: false } );

return Promise.resolve()
	.then(() => collection.remove({}, {many: true}) )
	.then(() => collection.insert(champions) )
	.then(() => db.close() );

