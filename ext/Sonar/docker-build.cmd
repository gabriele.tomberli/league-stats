docker build -t registry.gitlab.com/gabriele.tomberli/league-stats/sonarqube-scanner:test .
docker login registry.gitlab.com
docker push registry.gitlab.com/gabriele.tomberli/league-stats/sonarqube-scanner:test

echo Try out the new image using the "test" tag.
echo If working correctly, retag as "latest" and push.
