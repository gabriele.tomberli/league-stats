
## v0.2.3 (2018/08/25 14:39)
- Fix: Games are now registered in chronological order
- Fix: Midway fetching is now restored on subsequent start
- Fix: Fixed error in displaying fetching progress
## v0.2.0 (2017/11/14 20:42)
- New: Added recent games fetching
- New: Added league position fetching

