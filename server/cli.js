#!/usr/bin/env node

require( './init' )

const a = 1 // eslint-disable-line no-unused-vars
	, LeagueStats = globalRequire( 'src/league-stats' )
	, Command = globalRequire( 'src/lib/cli/command' )
	, cliProcess = globalRequire( 'src/lib/cli/process' )
	, fse = globalRequire( 'fs-extra' )
	, path = globalRequire( 'path' )
	;

/*
 * Main
 */

function fail( err ) {
	console.error();
	console.error( `Command execution failed for "${ process.argv.slice(2).join(' ') }": ` )
	console.error( err.stack );
	process.exit( 1 );
}

async function start() {

	let commandsPath = path.join( __dirname, 'src', 'cli', 'commands' );
	let commandFiles = await fse.readdir( commandsPath );
	let commands = commandFiles.map( file => {

		if ( !file.match(/cmd_.+\.js/) )
			return false;

		let commandPath = path.join( commandsPath, file );
		let command = require( commandPath );

		if ( !command ) {
			console.error( `Failed to include command from file ${ commandPath }: File does not export any object.` );
			return false;
		}

		if ( !( command instanceof Command ) ) {
			console.error( `Failed to include command from file ${ commandPath }: File does not export a valid Command object.` );
			return false;
		}

		return command;

	}).filter( Boolean );

	let secretKey = process.env.CONFIG_SECRET_KEY;
	if ( !secretKey ) {
		throw new Error( 'No CONFIG_SECRET_KEY found in environment.' );
	}

	let leagueStats = await LeagueStats.init( '.leaguestatsrc', secretKey );
	await cliProcess( process.argv.slice( 2 ), commands, { leagueStats } )
		.then( data => {
			if ( data.output )
				console.info( data.output );
			if ( data.error )
				return process.exit( 1 );
			process.exit( 0 );
		} );

}

/*
 *	Start
 */

start().catch( fail );
