

const requireLocal = function( moduleName ) {
	const req = require?.main?.require ?? require;
	return req( __dirname + '/' + moduleName );
}

/**
 * Function that makes it possible to require a local source file with an absolute path, instead of a relative one.
 * require( '../../../a' ) becomes globalRequire( 'src/a' ).
 * Only works by design on the src folder. Files outside cannot be required using this function.
 * This function is compatible with node external modules resolution and with absolute source files inside the src folder. Relative paths are not supported.
 */
globalThis.globalRequire = function( moduleName ) {
	if ( moduleName.trim().startsWith( '.' ) )
		throw new Error( 'Cannot use relative paths with globalRequire: ' + moduleName );
	let requireFn = moduleName.trim().startsWith( 'src/' ) ? requireLocal : require;
	return requireFn( moduleName );
}
