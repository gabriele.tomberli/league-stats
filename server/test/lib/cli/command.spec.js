
/* eslint-env mocha */
require( 'initTests' );
const chai = globalRequire( 'chai' )
	, expect = chai.use( globalRequire( 'chai-as-promised' ) ).expect
	, Command = globalRequire( 'src/lib/cli/command' )
	, Promise = global.Promise
	;

describe( 'CliCommand', function() {

	let mockedYargs;

	beforeEach( function() {
		mockedYargs = {}
	} );

	it( 'should throw an error if a command is not specified', function() {
		expect( () => new Command( null, 'x', () => {} ) ).to.throw( Error );
	} );

	it( 'should throw an error if a description is not specified', function() {
		expect( () => new Command( 'x', null, () => { } ) ).to.throw( Error );
	} );

	it( 'should throw an error if an handler is not specified', function() {
		expect( () => new Command( 'x', 'x', null ) ).to.throw( Error );
	} );

	it( 'should expose command, description and handler for Yargs integration', function() {
		const cmd = 'x', desc = 'just a test';
		let command = new Command( cmd, desc, () => 1 );
		expect( command.command ).to.be.eql( cmd );
		expect( command.description ).to.be.eql( desc );
		expect( command.handler ).to.be.instanceOf( Function );
	} );

	it( 'should place the result of the synchroneous execution as a Promise in yargs._resolve', async () => {
		let command = new Command( 'x', 'just a test', () => 1 );
		command.handler( mockedYargs );
		return expect( mockedYargs._resolve ).to.eventually.be.eql( 1 );
	} );

	it( 'should place the result of the asynchroneous execution as a Promise in yargs._resolve', async () => {
		let command = new Command( 'x', 'just a test', async () => new Promise( resolve => setTimeout( () => resolve(1), 500 ) ) );
		command.handler( mockedYargs );
		return expect( mockedYargs._resolve ).to.eventually.be.eql( 1 );
	} );

	it( 'should place itself in yargs._command', async () => {
		let command = new Command( 'x', 'just a test', () => 1 );
		command.handler( mockedYargs );
		return expect( mockedYargs._command ).to.be.eql( command );
	} );

} );
