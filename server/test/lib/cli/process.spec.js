
/* eslint-env mocha */
require( 'initTests' );
const chai = globalRequire( 'chai' )
	, expect = chai.use( globalRequire( 'chai-as-promised' ) ).expect
	, cliProcess = globalRequire( 'src/lib/cli/process' )
	, Command = globalRequire( 'src/lib/cli/command' )
	, Promise = global.Promise
	;

describe( 'CliProcess', function() {

	const COMMAND_OK = new Command( 'test-ok', 'test command', () => 'ok' );
	const COMMAND_KO = new Command( 'test-ko', 'test command', () => { throw new Error('ko'); } );
	const COMMAND_EMPTY = new Command( 'test-empty', 'test command', () => {} );
	const COMMAND_ARG1 = new Command( 'test-arg1 <x>', 'test command', yargs => { expect(yargs.x).to.eql(1); } );

	it('should throw an error if run without arguments', function() {
		return Promise.all( [
			expect( cliProcess() ).to.eventually.be.rejected,
			expect( cliProcess( null ) ).to.eventually.be.rejected,
			expect( cliProcess( undefined ) ).to.eventually.be.rejected
		] );
	} );

	it('should throw an error if run with invalid arguments', function() {
		return Promise.all( [
			expect( cliProcess( {} ) ).to.eventually.be.rejected,
			expect( cliProcess( 'a b' ) ).to.eventually.be.rejected
		] );
	} );

	it('should throw an error if run with invalid commands', function() {
		return Promise.all( [
			expect( cliProcess( 'x', {} ) ).to.eventually.be.rejected,
			expect( cliProcess( 'x', [ {} ] ) ).to.eventually.be.rejected
		] );
	} );

	it('should process a valid command', async function() {
		let ret = await cliProcess( [ 'test-ok' ], [ COMMAND_OK ] );
		expect( ret.error ).to.be.undefined;
		expect( ret.data ).to.be.eql( 'ok' );
		expect( ret.resolved ).to.be.true;
		expect( ret.command ).to.eql( COMMAND_OK );
	} );

	it( 'should process a valid command when using string syntax', async function() {
		let ret = await cliProcess( 'test-ok', [ COMMAND_OK ] );
		expect( ret.error ).to.be.undefined;
		expect( ret.resolved ).to.be.true;
		expect( ret.command ).to.eql( COMMAND_OK );
	} );

	it( 'should throw an error when processing a non existing command', async function() {
		let ret = await cliProcess( [ 'test-non-existant' ], [ COMMAND_OK ] );
		expect( ret.resolved ).to.be.false;
		expect( ret.command ).to.be.undefined;
		expect( ret.error ).to.be.an.instanceOf( Error );
		expect( ret.output ).to.be.not.empty;
	} );

	it( 'should throw an error when processing a failing command', async function() {
		let ret = await cliProcess( [ 'test-ko' ], [ COMMAND_KO ] );
		expect( ret.resolved ).to.be.false;
		expect( ret.command ).to.be.undefined; // Command execution failed with an Exception, no callback reached.
		expect( ret.error ).to.be.an.instanceOf( Error );
		expect( ret.output ).to.be.not.empty;
	} );

	it( 'should throw an error when processing a command with invalid args', async function() {
		let ret = await cliProcess( [ 'test-arg1' ], [ COMMAND_ARG1 ] );
		expect( ret.resolved ).to.be.false;
		expect( ret.command ).to.be.undefined;
		expect( ret.error ).to.be.an.instanceOf( Error );
		expect( ret.output ).to.be.not.empty;
	} );

	it( 'should process an existing command with valid args', async function() {
		let ret = await cliProcess( [ 'test-arg1', 1 ], [ COMMAND_ARG1 ] );
		expect( ret.error ).to.be.undefined;
		expect( ret.data ).to.be.undefined;
		expect( ret.resolved ).to.be.true;
		expect( ret.command ).to.eql( COMMAND_ARG1 );
	} );

	it( 'should process an empty command', async function() {
		let ret = await cliProcess( [ 'test-empty' ], [ COMMAND_EMPTY ] );
		expect( ret.error ).to.be.undefined;
		expect( ret.data ).to.be.undefined;
		expect( ret.resolved ).to.be.true;
		expect( ret.command ).to.eql( COMMAND_EMPTY );
	} );

});
