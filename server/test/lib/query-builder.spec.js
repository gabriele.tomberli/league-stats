
/* eslint-env mocha */
require( 'initTests' );
const chai = globalRequire( 'chai' ).use( globalRequire( 'chai-as-promised' ) )
	, expect = chai.expect
	, datefns = globalRequire( 'date-fns' )
	, QueryBuilder = globalRequire( 'src/lib/query-builder' )
	;


describe( 'QueryBuilder', function() {

	let qb;

	this.beforeEach( () => qb = new QueryBuilder( false ) );

	it( 'should return an empty query if no operation specified', function() {
		expect( qb.build() ).to.eql( {} );
	});

	it( 'should join operations in AND', function() {

		qb.equals( 'field1', 1 );
		qb.equals( 'field2', 2 );
		expect( qb.build() ).to.eql( { $and: [ { field1: { $eq: 1 } }, { field2: { $eq: 2 } } ] } );

	} );

	it( 'should allow chaining', function() {

		qb.equals( 'field1', 1 ).equals( 'field2', 2 );
		expect( qb.build() ).to.eql( { $and: [ { field1: { $eq: 1 } }, { field2: { $eq: 2 } } ] } );

	} );

	it( 'should allow filtering by dates', function() {

		let dt1 = new Date(),
			dt2 = new Date( 0 );

		const getUtcTime = dt => {
			const toUtcOffset = dt.getTimezoneOffset() * 60 * 1000;
			return new Date( dt.getTime() + toUtcOffset ).getTime();
		};

		let dt1midnight = getUtcTime( datefns.startOfDay( dt1 ) );
		let dt2midnight = getUtcTime( datefns.endOfDay( dt2 ) );

		qb.afterDate( 'date1', dt1 ).beforeDate( 'date1', dt2 );
		expect( qb.build() ).to.eql( { $and: [ { date1: { $gte: dt1midnight } }, { date1: { $lte: dt2midnight } } ] } );

	} );

	it( 'should allow subqueries', function() {

		qb.equals( 'field1', 1 ).or().equals( 'field2', 1 ).equals( 'field2', 2 );
		expect( qb.build() ).to.eql(
			{
				$and: [
					{ field1: { $eq: 1 } },
					{ $or: [ { field2: { $eq: 1 } }, { field2: { $eq: 2 } } ] }
				]
			}
		);

	} );

	it( 'should allow complex sub-operators', function() {

		qb.elemMatch( 'array' ).equals( 'field1', 1 ).equals( 'field2', 2 );
		expect( qb.build() ).to.eql(
			{
				$and: [
					{ array: { $elemMatch: { field1: { $eq: 1 }, field2: { $eq: 2 } } } }
				]
			}
		);

	} );

} );
