
/* eslint-env mocha */
require( 'initTests' );
const chai = globalRequire( 'chai' )
	, expect = chai.use( globalRequire( 'chai-as-promised' ) ).expect
	, lodash = globalRequire( 'lodash' )
	, DataUpgrader = globalRequire( 'src/lib/data-upgrader' )
	;

describe( 'DataUpgrader', function() {

	describe( 'createTransition', function() {

		let upgrader;

		beforeEach( () => upgrader = new DataUpgrader() );

		it( 'should return a transition object if from < to', function() {
			let transition1 = upgrader.createTransition( 1, 2, lodash.identity );
			let transition2 = upgrader.createTransition( 1, 2.5, lodash.identity );
			let transition3 = upgrader.createTransition( 1, 7, lodash.identity );
			expect( transition1 ).to.be.eql( {
				from: 1,
				to: 2,
				apply: lodash.identity
			} );
			expect( transition2 ).to.be.eql( {
				from: 1,
				to: 2.5,
				apply: lodash.identity
			} );
			expect( transition3 ).to.be.eql( {
				from: 1,
				to: 7,
				apply: lodash.identity
			} );
		} );

		it( 'should return a transition object if from < to, even if not sequential', function() {
			let transition = upgrader.createTransition( 1, 6, lodash.identity );
			expect( transition ).to.be.eql( {
				from: 1,
				to: 6,
				apply: lodash.identity
			} );
		} );

		it( 'should not allow a transition to be created if from == to', function() {
			expect( () => upgrader.createTransition( 1, 1, lodash.identity ) ).to.throw( Error );
		} );

		it( 'should not allow a transition to be created if from > to', function() {
			expect( () => upgrader.createTransition( 2, 1, lodash.identity ) ).to.throw( Error );
		} );

		it( 'should not allow a transition to be created with non-numeric versions', function() {
			expect( () => upgrader.createTransition( 'x', 1, lodash.identity ) ).to.throw( Error );
			expect( () => upgrader.createTransition( 1, 'y', lodash.identity ) ).to.throw( Error );
		} );

		it( 'should not allow a transition to be created with non-functional apply function', function() {
			expect( () => upgrader.createTransition( 1, 2, {} ) ).to.throw( Error );
		} );

		it( 'should not allow a transition to be created from a negative version', function() {
			expect( () => upgrader.createTransition( -1, 2, lodash.identity ) ).to.throw( Error );
		} );

		it( 'should not allow a transition to be created with duplicated from and to values', function() {
			upgrader.createTransition( 1, 2, lodash.identity )
			expect( () => upgrader.createTransition( 1, 2, lodash.identity ) ).to.throw( Error );
		} );

		it( 'should create immutable transitions', function() {
			let transition = upgrader.createTransition( 1, 2, lodash.identity );
			transition.from = 'x';
			transition.to = 'y';
			transition.apply = () => 1;
			expect( transition ).to.be.eql( {
				from: 1,
				to: 2,
				apply: lodash.identity
			} );
		} );

	} );

	describe( 'interface', function() {

		it( 'should hide mutator methods', function() {
			let upgrader = new DataUpgrader();
			expect( upgrader.createTransition ).to.be.a( 'function' );
			expect( upgrader.interface ).to.be.a( 'function' );
			expect( upgrader.upgrade ).to.be.a( 'function' );
			let upgraderInterface = upgrader.interface();
			expect( upgraderInterface.createTransition ).to.be.undefined;
			expect( upgraderInterface.interface ).to.be.undefined;
			expect( upgraderInterface.upgrade ).to.be.a( 'function' );
		} );

		it( 'should not allow ridefinitions', function() {
			let upgraderInterface = new DataUpgrader().interface();
			upgraderInterface.createTransition = lodash.noop;
			delete upgraderInterface.upgrade;
			expect( upgraderInterface.createTransition ).to.be.undefined;
			expect( upgraderInterface.interface ).to.be.undefined;
			expect( upgraderInterface.upgrade ).to.be.a( 'function' );
		} );

	} );

	describe( 'upgrade', function() {

		let data;
		let upgrader;

		beforeEach( function() {
			data = { x: 0, y: 0, z: 2, last: -1, _version: 1 };

			upgrader = new DataUpgrader( 4 );
			upgrader.createTransition( 0, 1, data => { data.zero = 1; data.last = 0; return data; } );
			upgrader.createTransition( 1, 2, data => { data.x = 1; data.last = 1; return data; } );
			upgrader.createTransition( 2, 3, data => { data.y = 2; data.last = 2; return data; } );
			upgrader.createTransition( 3, 4, data => { data.x = 2; data.last = 3; return data; } );
		} );

		it( 'should upgrade data using the available transitions when possible', async function() {
			const newData = await upgrader.upgrade( data, 2 );
			expect( newData.x ).to.be.eql( 1 );
			expect( newData.y ).to.be.eql( 0 );
			expect( newData.z ).to.be.eql( 2 );
			expect( newData._version ).to.be.eql( 2 );
		} );

		it( 'should upgrade data to the latest version if not specified otherwise', async function() {
			const newData = await upgrader.upgrade( data );
			expect( newData.x ).to.be.eql( 2 );
			expect( newData.y ).to.be.eql( 2 );
			expect( newData.z ).to.be.eql( 2 );
			expect( newData._version ).to.be.eql( 4 );
		} );

		it( 'should consider data version 0 if specified', async function() {
			const newData = await upgrader.upgrade( data );
			expect( newData.zero ).to.be.undefined;
			expect( newData._version ).to.be.eql( 4 );
		} );

		it( 'should consider data at version 0 if not specified otherwise', async function() {
			delete data._version;
			const newData = await upgrader.upgrade( data );
			expect( newData.zero ).to.be.eql( 1 );
			expect( newData._version ).to.be.eql( 4 );
		} );

		it( 'should not modify the original values', async function() {
			const newData = await upgrader.upgrade( data );
			expect( data.x ).to.not.be.eql( newData.x );
			expect( data.y ).to.not.be.eql( newData.y );
		} );

		it( 'should do nothing if the original version is greater than the last version', async function() {
			data._version = 6;
			const n1 = await upgrader.upgrade( data );
			const n2 = await upgrader.upgrade( data, 5 ); // A path to 5 does not exist, but we don't care in this case.
			expect( n1._version ).to.be.eql( 6 );
			expect( n2._version ).to.be.eql( 6 );
		} );

		it( 'should throw an error if a path to the given version does not exist', async function() {
			return expect( upgrader.upgrade( data, 5 ) ).to.eventually.be.rejectedWith( DataUpgrader.UnavailableUpgradeError );
		} );

		it( 'should not permit transitions that result in undefined data', async function() {
			upgrader.createTransition( 4, 5, () => {} );
			return expect( upgrader.upgrade( data ) ).to.eventually.be.rejectedWith( DataUpgrader.EmptyUpgradeError );
		} );

		it( 'should not permit upgrading null or undefined data', async function() {
			expect( upgrader.upgrade( null ) ).to.eventually.be.rejectedWith( Error );
			expect( upgrader.upgrade( undefined ) ).to.eventually.be.rejectedWith( Error );
		} );

		describe( 'shortest path', function() {

			beforeEach( function() {
				data.skipped = 0;
				upgrader.createTransition( 4, 5, data => { data.skipped += 0; return data; } );
				upgrader.createTransition( 5, 6, data => { data.skipped += 0; return data; } );
				upgrader.createTransition( 6, 7, data => { data.skipped += 0; return data; } );
				upgrader.createTransition( 7, 8, data => { data.skipped += 0; return data; } );
				upgrader.createTransition( 8, 9, data => { data.skipped += 0; return data; } );
				upgrader.createTransition( 9, 10, data => { data.skipped += 0; return data; } );
			} );

			it( 'will include all steps if no shorter path is present', async function() {
				// Only one path is present which passes from 1 to 10 in 10 steps.
				const newData = await upgrader.upgrade( data );
				expect( newData._version ).to.be.eql( 10 );
				expect( newData.skipped ).to.be.eql( 0 );
			} );

			it( 'will skip unnecessary steps when possible', async function() {
				// There are multiple ways to reach 10:
				// 1 - 2 - 3 - 4 - 5 - 6 - 7 - 8 - 9 - 10 (10 steps, skipped 0)
				// 1 - 2 - 5 - 6 - 7 - 8 - 9 - 10 (8 steps, skipped 2)
				// 1 - 2 - 5 - 8 - 9 - 10 (6 steps, skipped 4)
				// 1 - 2 - 3 - 4 - 5 - 8 - 9 - 10 (8 steps, skipped 2)
				// 1 - 2 - 6 - 7 - 8 - 9 - 10 (7 steps, skipped 3)
				// 1 - 2 - 3 - 7 - 8 - 9 - 10 (7 steps, skipped 3)
				// The algorithm should use the shortest path to reach its destination, thus the one giving the most skipped steps.
				upgrader.createTransition( 2, 5, data => { data.skipped += 2; return data; } );
				upgrader.createTransition( 3, 7, data => { data.skipped += 3; return data; } );
				upgrader.createTransition( 5, 8, data => { data.skipped += 2; return data; } );
				upgrader.createTransition( 2, 6, data => { data.skipped += 3; return data; } );
				const newData = await upgrader.upgrade( data );
				expect( newData._version ).to.be.eql( 10 );
				expect( newData.skipped ).to.be.eql( 4 );
			} );

			it( 'should ignore unreachable destinations', async function() {
				upgrader.createTransition( 2.5, 10, data => { data.skipped += 6.5; return data; } );
				const newData = await upgrader.upgrade( data );
				expect( newData._version ).to.be.eql( 10 );
				expect( newData.skipped ).to.be.eql( 0 );
			} );

		} );

	} );

} );
