
/* eslint-env mocha */
require( 'initTests' );
const chai = globalRequire( 'chai' ).use( globalRequire( 'chai-as-promised' ) )
	, expect = chai.expect
	, MatchFetcher = globalRequire( 'src/modules/fetchers/match-fetcher' )
	, test = globalRequire( 'src/lib/test' )
	;


describe( 'MatchFetcher', function() {

	const td = globalRequire( 'testdouble' )
	afterEach( function() {
		td.reset()
	} )

	it( 'should be created even without dependencies', function() {
		expect( () => new MatchFetcher() ).to.not.throw();
	} );

	describe( '_percentage', function() {

		let matchFetcher = new MatchFetcher()
			, percentage = test.retrieve( matchFetcher, 'percentage' );

		it( 'should not print anything when retrieved matches are less than 10', function() {
			Array( 10 ).fill( 0 ).forEach( (v, k) => {
				expect( percentage(1, k) ).to.equal( '', `Percentage should be empty when invoked with ${k} results` );
			} );
		} );

		it( 'should print "100.00%. " on last iteration', function() {
			expect( percentage( 100, 100 ) ).to.equal( '100.00%. ' );
		} );

	} );

	describe( '_alreadyRegisteredSummoners', function() {

		let usersStoreMock = td.object()
			, user1 = { accountId: '1', name: 'a' }
			, user2 = { accountId: '2', name: 'b' }
			, user3 = { accountId: '3', name: 'c' }
			, matchFetcher
			, alreadyRegisteredSummoners
			;

		beforeEach( function() {
			matchFetcher = new MatchFetcher( null, null, usersStoreMock, null )
			td.when(usersStoreMock.getAllUsers()).thenReturn( [ user1, user2 ] );

			alreadyRegisteredSummoners = test.retrieve( matchFetcher, 'alreadyRegisteredSummoners' )
		} );

		it( 'should output an empty string when no summoner is registered', function() {
			return expect( alreadyRegisteredSummoners( [] ) ).to.eventually.equal( '' );
		} );

		it( 'should output the summoner name of the single registered summoner', function() {
			return expect( alreadyRegisteredSummoners( [ user1.accountId ] ) ).to.eventually.equal( user1.name );
		} );

		it( 'should output all available summoner names if there are more than one registered', function() {
			return expect( alreadyRegisteredSummoners( [ user1.accountId, user2.accountId ] ) ).to.eventually.equal( `${ user1.name }, ${ user2.name }` );
		} );

		it( 'should output the account ID for unregistered summoners', function() {
			return expect( alreadyRegisteredSummoners( [ user3.accountId ] ) ).to.eventually.equal( user3.accountId );
		} );

	} );

} );
