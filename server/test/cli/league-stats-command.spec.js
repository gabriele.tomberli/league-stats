
/* eslint-env mocha */
require( 'initTests' );
const chai = globalRequire( 'chai' )
	, expect = chai.use( globalRequire( 'chai-as-promised' ) ).expect
	, LeagueStatsCommand = globalRequire( 'src/cli/league-stats-command' )
	;

describe( 'CliLeagueStatsCommand', function() {

	let activated, deactivated, executed;
	let mockedLeagueStats = {
		activate: async() => activated = true,
		deactivate: async() => deactivated = true
	};
	let mockedYargs;

	beforeEach( function() {
		activated = false;
		deactivated = false;
		executed = false;
		mockedYargs = { leagueStats: mockedLeagueStats };
	} );

	it( 'should be a Command', function() {
		expect( new LeagueStatsCommand( 'x', 'just a test', _ => {} ) ).to.be.instanceOf( globalRequire( 'src/lib/cli/command' ) );
	} );

	it( 'should execute the given function with a valid LeagueStats instance', async () => {
		let command = new LeagueStatsCommand( 'x', 'just a test', ls => {
			executed = true;
			expect( ls ).to.be.eql( mockedLeagueStats );
		} );
		command.handler( mockedYargs );
		await mockedYargs._resolve;
		expect( executed ).to.be.true;
	} );

	it( 'should activate and deactivate LeagueStats in between handler execution', async () => {
		let command = new LeagueStatsCommand( 'x', 'just a test', () => executed = true );
		expect( deactivated ).to.be.false;
		expect( activated ).to.be.false;
		expect( executed ).to.be.false;
		command.handler( mockedYargs );
		await mockedYargs._resolve;
		expect( executed ).to.be.true;
		expect( activated ).to.be.true;
		expect( deactivated ).to.be.true;
	} );

	it( 'should throw an error if a command is not specified', function() {
		expect( () => new LeagueStatsCommand( null, 'x', () => { } ) ).to.throw( Error );
	} );

	it( 'should throw an error if a description is not specified', function() {
		expect( () => new LeagueStatsCommand( 'x', null, () => { } ) ).to.throw( Error );
	} );

	it( 'should throw an error if an handler is not specified', function() {
		expect( () => new LeagueStatsCommand( 'x', 'x', null ) ).to.throw( Error );
	} );

	it( 'should place the result of the execution as a Promise in yargs._resolve', async () => {
		let command = new LeagueStatsCommand( 'x', 'just a test', () => 1 );
		command.handler( mockedYargs );
		return expect( mockedYargs._resolve ).to.eventually.be.eql( 1 );
	} );

	it( 'should place itself in yargs._command', async () => {
		let command = new LeagueStatsCommand( 'x', 'just a test', () => 1 );
		command.handler( mockedYargs );
		return expect( mockedYargs._command ).to.be.eql( command );
	} );

} );
