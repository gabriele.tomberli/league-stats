
/* eslint-env mocha */
require( 'initTests' );
const chai = globalRequire( 'chai' ).use( globalRequire( 'chai-as-promised' ) )
	, expect = chai.expect
	, should = chai.should()
	, nock = globalRequire( 'nock' )
	, Promise = globalRequire( 'bluebird' )
	, RestService = globalRequire( 'src/core/rest-service' )
	;


describe( 'RestService', function() {

	describe( 'request', function() {

		const BASE_URL = 'https://<endpoint>/lol/';
		const API_KEY = 'API_KEY';
		const PLATFORM_ID = 'euw1';

		// Riot Games enforces a max 5 requests per second limit
		const MAX_CALLS_PER_SECOND = 5;

		// Milliseconds to backoff between calls.
		const MIN_DELAY_BETWEEN_CALLS = 50;

		// On reaching Riot' limits, be overzealous in trying to respect them
		const RIOT_LIMIT_BACKOFF_TIME = 150;

		// The number of retries to make on REST errors
		const MAX_RETRIES = 3;

		// Considering the limit of 5 requests per 1000ms, requests should be made every 200ms to be optimal.
		const optimalCallDelay = 1000 / MAX_CALLS_PER_SECOND;

		// Disable all non-mocked connections.
		nock.disableNetConnect();

		// Create a new RestService.
		let restService = new RestService( BASE_URL, API_KEY, {
			maxCallsPerSec: MAX_CALLS_PER_SECOND,
			minCallDelay: MIN_DELAY_BETWEEN_CALLS,
			maxRetriesOnError: MAX_RETRIES,
			backOffTime: RIOT_LIMIT_BACKOFF_TIME
		} );

		this.beforeEach( function() {
			nock.cleanAll();
		} );

		it( 'should make calls to the right endpoint', function() {

			nock( 'https://euw1.api.riotgames.com' )
				.get( '/lol/test' )
				.reply( 200, 'ok' );

			const request = restService.request( PLATFORM_ID, 'test' );
			return Promise.all( [
				request.then( res => res.status ).should.eventually.equal( 200 ),
				request.then( res => res.data ).should.eventually.equal( 'ok' )
			] );

		} );

		it( 'should throw an error on invalid platform', function() {
			const invalidPlatformId = PLATFORM_ID + 'x';
			return should.throw( () => restService.request( invalidPlatformId, 'test' ), Error, `Invalid platform given: ${ invalidPlatformId }.` );
		} );

		it( 'should not flood the API endpoint', function() {

			const seconds = 2;
			const requestCount = 1 + ( MAX_CALLS_PER_SECOND * seconds );

			this.slow( seconds * 2 * 1000 );
			this.timeout( seconds * 3 * 1000 );

			let start = new Date(),
				last = start;

			nock( 'https://euw1.api.riotgames.com' )
				.get( '/lol/flood' )
				.times( requestCount )
				.reply( 200, () => {
					const now = new Date().getTime(), ret = now - last;
					last = now;
					return ret;
				} );

			const requests = Array.apply( null, { length: requestCount } ).map( () => restService.request( PLATFORM_ID, 'flood' ) );
			return Promise.all( requests )
				// skip the first request
				.filter( ( item, index ) => index > 0 )
				// get the time passed since the previous request.
				.map( res => res.data )
				.each( diff => diff.should.be.above( MIN_DELAY_BETWEEN_CALLS ) )
				.then( () => ( new Date().getTime() - start ).should.be.above( seconds * 1000 ) );

		} );

		it( 'should respect RiotGames\' limits', function() {

			const RETRY_AFTER = 1;
			this.slow( optimalCallDelay + optimalCallDelay + RIOT_LIMIT_BACKOFF_TIME + ( RETRY_AFTER * 1000 ) + 250 );

			nock( 'https://euw1.api.riotgames.com' )
				.get( '/lol/limit' )
				.reply( 429, '', { 'retry-after': RETRY_AFTER } );
			nock( 'https://euw1.api.riotgames.com' )
				.get( '/lol/limit' )
				.reply( 200, 'ok' );

			let request = restService.request( PLATFORM_ID, 'limit' );
			return request.then( res => Promise.all( [
				res.data.should.equal( 'ok' ),
				nock.isDone().should.be.true
			] ) );

		} );

		it( 'should bail out on too many consecutive errors', function() {

			this.slow( ( optimalCallDelay * MAX_RETRIES ) + ( RIOT_LIMIT_BACKOFF_TIME * MAX_RETRIES ) + 250 );

			nock( 'https://euw1.api.riotgames.com' )
				.get( '/lol/permanent' )
				.times( MAX_RETRIES + 1 )
				.reply( 500 );

			let request = restService.request( PLATFORM_ID, 'permanent' );
			return request.then( x => should.fail(), err => {
				expect( err.statusCode ).to.equal( 500 );
				expect( nock.isDone() ).to.be.true;
			} );

		} );

		it( 'should retry calls on server issues', function() {

			this.slow( optimalCallDelay + optimalCallDelay + RIOT_LIMIT_BACKOFF_TIME + 250 );

			nock( 'https://euw1.api.riotgames.com' )
				.get( '/lol/server' )
				.reply( 503 );
			nock( 'https://euw1.api.riotgames.com' )
				.get( '/lol/server' )
				.reply( 200, 'ok' );

			let request = restService.request( PLATFORM_ID, 'server' );
			return request.then( res => Promise.all( [
				res.data.should.equal( 'ok' ),
				nock.isDone().should.be.true
			] ) );
		} );

		it( 'should not retry calls on request issues', function() {

			nock( 'https://euw1.api.riotgames.com' )
				.get( '/lol/client' )
				.reply( 400 );

			let request = restService.request( PLATFORM_ID, 'client' );
			return request.then( x => should.fail(), res => res.statusCode.should.equal( 400 ) )
		} );

		it( 'should return a filterable response by statusCode', function() {

			nock( 'https://euw1.api.riotgames.com' )
				.get( '/lol/found' )
				.reply( 200 );
			nock( 'https://euw1.api.riotgames.com' )
				.get( '/lol/not-found' )
				.reply( 404 );

			const wrap = promise => promise.then( () => false ).catch( { statusCode: 404 }, err => true );
			return Promise.all( [
				wrap( restService.request( PLATFORM_ID, 'found' ) ).should.eventually.be.false,
				wrap( restService.request( PLATFORM_ID, 'not-found' ) ).should.eventually.be.true
			] );
		} );

		it( 'should pass the correct app token to the endpoint', function() {

			nock( 'https://euw1.api.riotgames.com' )
				.get( '/lol/key' )
				.matchHeader( 'x-riot-token', API_KEY )
				.reply( 200 );

			return restService.request( PLATFORM_ID, 'key' ).should.eventually.be.fulfilled;

		} );

	} );

} );
