
const Command = globalRequire( 'src/lib/cli/command' );

const wrap = function( fn ) {
	
	if ( fn === null || fn === undefined )
		return null;

	return async function( yargs ) {
		let leagueStats = yargs.leagueStats;

		await leagueStats.activate();
		try {
			return await fn( leagueStats, yargs );
		} finally {
			try {
				await leagueStats.deactivate();
			} catch (e) {
				console.warn( 'Failed to deactivate LeagueStats after command execution.' );
			}
		}
	}
}

module.exports = class LeagueStatsCommand extends Command {

	constructor( command, description, handler, wrapInLeagueStats = true ) {
		super( command, description, wrapInLeagueStats ? wrap( handler ) : handler );
	}

}
