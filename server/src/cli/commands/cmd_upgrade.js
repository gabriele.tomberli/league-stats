
const a = 1 // eslint-disable-line no-unused-vars
	, LeagueStatsCommand = globalRequire( 'src/cli/league-stats-command' )
	;

class Cmd extends LeagueStatsCommand {

	constructor() {
		super(
			'upgrade',
			'Upgrade existing data to latest version',
			( leagueStats, yargs ) => this.upgrade( leagueStats )
		);
	}

	async upgrade( ls ) {
		let [ matchFetcher ] = await ls.resolve( [ 'matchFetcher' ] );
		let promises = [
			matchFetcher.upgradeOutdatedData()
		];
		return global.Promise.all( promises );
	}

}

module.exports = new Cmd();
