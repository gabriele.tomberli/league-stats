
const a = 1 // eslint-disable-line no-unused-vars
	, LeagueStatsCommand = globalRequire( 'src/cli/league-stats-command' )
	, path = globalRequire( 'path' )
	;

class Cmd extends LeagueStatsCommand {

	constructor() {
		super( 'users', 'Commands related to user management', () => {}, false );
	}

	builder( yargs ) {
		return yargs
			.commandDir( path.join( __dirname, 'users' ) )
			.demandCommand( 1 );
	}

}

module.exports = new Cmd();

