
const a = 1 // eslint-disable-line no-unused-vars
	, LeagueStatsCommand = globalRequire( 'src/cli/league-stats-command' )
	;

class Cmd extends LeagueStatsCommand {

	constructor() {
		super(
			'serve',
			'Serves data through the LS Rest APIs',
			( leagueStats, yargs ) => this.serve( leagueStats )
		);
	}

	async serve( ls ) {
		let [ server ] = await ls.resolve( [ 'server' ] );
		server.activate();

		return new global.Promise( ( resolve, reject ) => { } );
	}

}

module.exports = new Cmd();
