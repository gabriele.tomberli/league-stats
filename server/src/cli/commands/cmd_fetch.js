
const a = 1 // eslint-disable-line no-unused-vars
	, LeagueStatsCommand = globalRequire( 'src/cli/league-stats-command' )
	, Bluebird = globalRequire( 'bluebird' )
	;

const FETCH_RECENT_MATCHES = {
	id: 'recents',
	aliases: [ 'matches' ],
	handler: fetchRecentMatches
};
const FETCH_RANKED_POSITIONS = {
	id: 'leagues',
	aliases: [ 'rankeds', 'positions' ],
	handler: fetchRankedPositions
};
const FETCH_SUMMONERS = {
	id: 'summoners',
	handler: fetchSummoners
};
const allFetchTypes = [ FETCH_SUMMONERS, FETCH_RECENT_MATCHES, FETCH_RANKED_POSITIONS ];

const FETCH_ALL = {
	id: 'all',
	handler: async ( ...args ) => Bluebird.each( allFetchTypes, fetch => fetch.handler.apply( null, args ) )
};

class Cmd extends LeagueStatsCommand {

	constructor() {
		super(
			'fetch [what] [for]',
			'Fetches and store new data from the League of Legends API',
			( leagueStats, yargs ) => this.fetch( leagueStats, yargs.what, yargs.for, yargs.forced )
		);
	}

	builder( yargs ) {
		return yargs
			.positional( 'what', {
				description: 'Select what to fetch',
				default: FETCH_ALL.id,
				type: 'string',
				coerce: value => {
					let commands = [ FETCH_ALL, ...allFetchTypes ].reduce( ( ret, cur ) => {
						[ cur.id, ...cur.aliases || [] ].forEach( key => ret[ key ] = cur );
						return ret;
					}, {} );
					if ( !commands[ value ] ) {
						throw new Error( 'Invalid fetch type, valid arguments are: ' + Object.keys( commands ).sort().join( ', ' ) );
					}
					return commands[ value ];
				}
			} )
			.positional( 'for', {
				description: 'Restricts which summoners to fetch',
				type: 'string'
			} )
			.option( 'forced', {
				type: 'boolean',
				default: false,
				description: 'If true, gets all matches instead of only the most recent ones'
			} );
	}

	async fetch( ls, what, forAccount, forced ) {
		let usersStore = await ls.resolve( 'usersStore' );
		let users = await calculateUsers( usersStore, forAccount );
		return await what.handler( ls, users, forced );
	}

}

async function calculateUsers( usersStore, forAccount ) {
	let users;

	if ( forAccount ) {
		let user = await usersStore.getByName( forAccount );
		if ( !user )
			throw new Error( `No user found with name '${ forAccount }'.` );
		users = [ user ];
	} else {
		users = await usersStore.getAllUsers();
	}

	return users;
}

async function fetchRecentMatches( ls, users, forced ) {
	let [ matchFetcher ] = await ls.resolve( [ 'matchFetcher' ] );
	return Bluebird.each( users, user => matchFetcher.fetchAndStoreUserRecentGames( user, forced ) );
}

async function fetchRankedPositions( ls, users ) {
	let [ positionFetcher ] = await ls.resolve( [ 'positionFetcher' ] );
	return Bluebird.each( users, user => positionFetcher.fetchUserPosition( user ) );
}

async function fetchSummoners( ls, users ) {
	let [ summonerFetcher ] = await ls.resolve( [ 'summonerFetcher' ] );
	return Bluebird.each( users, user => summonerFetcher.fetchSummoner( user ) );
}

module.exports = new Cmd();
