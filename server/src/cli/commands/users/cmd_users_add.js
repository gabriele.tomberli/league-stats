
const a = 1 // eslint-disable-line no-unused-vars
	, LeagueStatsCommand = globalRequire( 'src/cli/league-stats-command' )
	, { UserNotFoundError } = globalRequire( 'src/errors' )
	;

class Cmd extends LeagueStatsCommand {

	constructor() {
		super(
			'add <platformId> <summonername>',
			'Adds a new user to the application',
			( leagueStats, yargs ) => this.addUser( leagueStats, yargs.platformId, yargs.summonername )
		);
	}

	builder( yargs ) {
		return yargs
			.positional( 'platformId', {
				type: 'string',
				alias: [ 'platform' ],
				description: 'The ID of the platform'
			} )
			.positional( 'summonername', {
				type: 'string',
				description: 'The name of the summoner'
			} );
	}

	async addUser( ls, platformId, summonername ) {
		let [ summonersApi, usersStore ] = await ls.resolve( [ 'summonersApi', 'usersStore' ] );

		if ( await usersStore.getByName( summonername ) ) {
			console.error( 'User already registered: ', summonername );
			return null;
		}

		let summoner = await summonersApi.fetchSummonerByName( platformId, summonername )
			.catch( UserNotFoundError, e => {
				console.error( 'User not found: ', summonername );
				return null;
			} );

		if ( !summoner )
			return;

		return usersStore.insertUser( summoner )
			.then( () => console.info( 'User added: ', summoner ) )
			.catch( e => console.error( 'Unable to add user: ' + e.message ) );
	}

}

module.exports = new Cmd();
