
const a = 1 // eslint-disable-line no-unused-vars
	, LeagueStatsCommand = globalRequire( 'src/cli/league-stats-command' )
	, { Platforms } = globalRequire( 'src/enums' )
	, { ary, map, orderBy } = globalRequire( 'lodash' )
	;

class Cmd extends LeagueStatsCommand {

	constructor() {
		super(
			'list [platformId]',
			'Lists all registered users',
			( leagueStats, yargs ) => this.listUsers( leagueStats, yargs.platformId, yargs.summonername )
		);
	}

	get aliases() {
		return 'ls';
	}

	builder( yargs ) {
		return yargs
			.positional( 'platformId', {
				type: 'string',
				alias: [ 'platform' ],
				description: 'The ID of the platform, all platforms by default'
			} );
	}

	async listUsers( ls, platformId ) {
		let [ usersStore ] = await ls.resolve( [ 'usersStore' ] );

		let filters = {};
		if ( platformId ) {
			let platform = Platforms[ platformId.toUpperCase() ];
			if ( !platform )
				throw new Error( `Invalid platform given: ${ platformId }.` );
			filters.platformId = platform.key;
		}

		return global.Promise.resolve( filters )
			.then( usersStore.getAllUsers )
			.then( users => orderBy( users, 'name' ) )
			.then( users => map( users, 'name' ) )
			.then( names => names.forEach( ary( console.info, 1 ) ) );

	}

}

module.exports = new Cmd();
