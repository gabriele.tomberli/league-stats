
const axios = globalRequire( 'axios' )
	, lodash = globalRequire( 'lodash' )
	, Throttler = globalRequire( '@zelgadis87/throttler' )
	, { Platforms } = globalRequire( 'src/enums' )
	;

class RestError extends Error {

	constructor( statusCode, message ) {
		super( message );
		this.statusCode = statusCode;
	}

}

const logRequests = false;
const logResponses = false;

function RestService( baseUrl, apiKey, restOpts = {} ) {

	const me = this;

	// Interface
	me.request = request;

	// Implementation

	restOpts = lodash.merge( {
		maxCallsPerSec: 5, // How many requests to make at most per second.
		minCallDelay: 50, // Milliseconds to backoff between calls. // XXX: Currently not handled.
		maxRetriesOnError: 3,
		backOffTime: 2500
	}, restOpts );

	let throttler = new Throttler( 1, 1000 / restOpts.maxCallsPerSec ); // XXX: RiotGames API limits are actually a bit more complex..

	function request( platformId, endpoint, opt, useRoutingValue = false ) {

		let platform = Platforms.platform( platformId );
		if ( platform === Platforms.UNDEFINED )
			throw new Error( `Invalid platform given: ${ platformId }.` );

		let url = baseUrl.replace( '<endpoint>', useRoutingValue ? platform.routingEndpoint : platform.endpoint ) + endpoint;
		let options = lodash.merge( {}, opt, {
			url: url,
			resolveWithFullResponse: true,
			json: true,
			headers: {
				'X-Riot-Token': apiKey
			},
			retryLimit: restOpts.maxRetriesOnError
		} );

		if ( logRequests )
			console.debug( 'Requesting data from: ', url, opt?.params ?? {} );
		const respPromise = doRequest( options );
		if ( logResponses )
			respPromise.then( resp => console.debug( 'Response: ', resp ) );
		return respPromise;
	}

	function doRequest( options ) {
		return throttler.throttle( () => axios.request( options ) ).catch( reason => handleRequestError( reason, options ) );
	}

	function handleRequestError( reason, options ) {

		if ( !reason.response.status )
			throw reason;

		let currentTry = options.currentTry || 0;
		let retry = false, delay = restOpts.backOffTime * (1 + (currentTry * 1.25)), { status : statusCode, statusText } = reason.response;
		let error = new RestError( statusCode, `API request to ${ options.url } failed with: ${ statusCode } ${ statusText }.` );

		if ( statusCode === 429 ) {
			// Handle RIOT API limits.
			retry = true;
		} else if ( statusCode >= 500 && statusCode < 600 ) {
			// Handle server temporary failures.
			retry = true;
		}

		if ( reason.response.headers[ 'retry-after' ] ) {
			delay += parseInt( reason.response.headers[ 'retry-after' ] ) * 1000;
		}

		if ( retry ) {
			let retriesLeft = options.retryLimit - currentTry, retryMessage;
			if ( retriesLeft > 0 ) {
				retryMessage = `Waiting ${ delay }ms before retrying. (${ retriesLeft }/${ options.retryLimit } tries left).`;
			} else {
				retryMessage = `No more retries left.`;
				retry = false;
			}
			console.warn( error.message + ' ' + retryMessage );
		}

		if ( retry) {
			let newOptions = { ...options, currentTry: currentTry + 1 };
			return new global.Promise( resolve => {
				setTimeout( () => resolve( doRequest( newOptions ) ), delay );
			} );
		} else {
			throw error;
		}

	}

	return me;

}

module.exports = RestService;
