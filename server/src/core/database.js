
const monk = globalRequire( 'monk' )
	;

function Database( url ) {

	let me = this;

	// Interface

	me.activate = activate;
	me.dropDatabase = dropDatabase;
	me.close = close;
	me.create = create;
	me.get = get;

	// Implementation

	let db;

	function activate() {
		db = monk( url );
		return db.then( () => me );
	}

	function dropDatabase() {
		return db._db.dropDatabase();
	}

	function close() {
		return db.close();
	}

	function create( name, creationOptions, opts ) {
		return db.create( name, creationOptions, opts );
	}

	function get( name, opts ) {
		return db.get( name, opts );
	}

	return me;

}

module.exports = Database;
