
const Koa = globalRequire( 'koa' )
	, KoaRouter = globalRequire( 'koa-router' )
	, koaLogger = globalRequire( 'koa-logger' )
	;

function Server() {

	let me = this;

	// Interface
	me.activate = activate;
	me.deactivate = deactivate;
	me.router = router;

	// Implementation

	let activated = false,
		routers = {},
		shutdown = () => {};

	function activate( port = 3030 ) {

		if ( activated )
			throw new Error( 'App already started' );
		activated = true;

		const app = new Koa();

		app.use( _setUpRouter().routes() );
		app.use( koaLogger() )

		// Start the server on port 3030
		const server = app.listen( port );
		server.on( 'listening', () => console.log( `Koa REST API started at http://localhost:${ port }` ) );

		shutdown = () => {
			server.close();
			activated = false;
		};

	}

	function deactivate() {
		shutdown();
	}

	function router(path) {
		let pathRouter = new KoaRouter();
		routers[ path ] = pathRouter;
		return pathRouter;
	}

	function _setUpRouter() {
		let mainRouter = new KoaRouter();
		for (let path in routers) {
			mainRouter.use( path, routers[path].routes() );
		}
		return mainRouter;
	}

	return me;

}

module.exports = Server;

