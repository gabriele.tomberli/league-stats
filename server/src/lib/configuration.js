

const a = 1 // eslint-disable-line no-unused-vars
	, crypto = globalRequire( 'crypto' )
	, cryptoAlgorithm = 'aes-256-ctr'
	, fs = globalRequire( 'fs' )
	, lodash = globalRequire( 'lodash' )
	, yaml = globalRequire( 'js-yaml' )
	, yargs = globalRequire( 'yargs/yargs' )
	, Promise = global.Promise
	;

class InvalidSecretKey extends Error { }

function asyncReadFile( filepath, opts = 'utf8' ) {
	return new Promise( ( res, rej ) => {
		fs.readFile( filepath, opts, ( err, data ) => {
			if ( err ) rej( err )
			else res( data )
		} )
	} );
}

function asyncWriteFile( filepath, data, opts = 'utf8' ) {
	return new Promise( ( resolve, reject ) => {
		fs.writeFile( filepath, data, opts, ( err, ret ) => {
			if ( err ) reject( err )
			else resolve( ret )
		} )
	} );
}

function encrypt( text, key ) {
	var cipher = crypto.createCipher( cryptoAlgorithm, key )
	var crypted = cipher.update( text, 'utf8', 'hex' )
	crypted += cipher.final( 'hex' );
	return crypted;
}

function decrypt( text, key ) {
	var decipher = crypto.createDecipher( cryptoAlgorithm, key )
	var dec = decipher.update( text, 'hex', 'utf8' )
	dec += decipher.final( 'utf8' );
	return dec;
}

function environmentOverride( data, prefix = null ) {

	let argv = yargs.Parser( process.argv.slice( 2 ) );
	lodash.each( data, ( v, k ) => {

		let key = ( prefix ? `${ prefix }_` : '' ) + k.toUpperCase();
		if ( lodash.isPlainObject( v ) ) {
			data[ k ] = environmentOverride( v, key );
		} else if ( argv[ key ] ) {
			data[ k ] = argv[ key ];
		} else if ( process.env[ key ] ) {
			data[ k ] = process.env[ key ];
		} else {
			data[ k ] = v;
		}

	} );

	return data;

}

function Configuration() {

	let configuration = this;

	configuration.init = ( filepath, secretKey ) => {

		let encFilepath = `${ filepath }.enc`
			, config = {};

		configuration.reload = async () => {

			if ( !fs.existsSync( filepath ) ) {
				let encData = await asyncReadFile( encFilepath );

				let data = decrypt( encData, secretKey );
				if ( data[ 0 ] !== '#' )
					throw new InvalidSecretKey( 'Could not decrypt configuration file using given key.' );

				config = yaml.load( data );
				await asyncWriteFile( filepath, data );
			} else {
				let data = await asyncReadFile( filepath );
				config = yaml.load( data );
			}

			configuration.data = Object.freeze( environmentOverride( config ) );

			return configuration;

		}

		configuration.get = key => Object.freeze( lodash.get( config, key ) );

		configuration.save = async ( key = secretKey ) => {
			let data = `###\n${ yaml.safeDump( config ) }`;
			let encData = encrypt( data, key );
			await asyncWriteFile( encFilepath, encData );
			await asyncWriteFile( filepath, data );
			return configuration;
		}

		configuration.data = {};

		return configuration.reload();

	};

	return configuration;

}

module.exports = new Configuration();
