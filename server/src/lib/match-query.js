
const a = 1 // eslint-disable-line no-unused-vars
	, QueryBuilder = globalRequire( 'src/lib/query-builder' )
	;

function MatchQuery( ) {

	let me = this;

	const qb = new QueryBuilder( 'matches' );

	// Interface
	me.build = qb.build;
	me.createdAfter = value => qb.afterDate( 'gameCreation', value );
	me.createdBefore = value => qb.beforeDate( 'gameCreation', value );
	me.player = ({ accountId, championId } = { accountId, championId }) => qb.elemMatch( 'participants' ).equals( '_accountId', accountId ).equals( 'championId', championId );

	return me;

}

module.exports = MatchQuery;
