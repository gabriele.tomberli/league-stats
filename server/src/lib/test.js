
const process = globalRequire( 'process' )
	, env = process.env.NODE_ENV || 'dev'
	;

function expose( obj, fn, name = fn.name ) {

	if ( typeof obj !== 'object' ) /* instanbul ignore if */
		throw new Error( 'Obj: object expected, got: ' + typeof name );
	if ( typeof fn !== 'function' ) /* instanbul ignore if */
		throw new Error( 'Fn: function expected, got: ' + typeof name );
	if ( typeof name !== 'string' ) /* instanbul ignore if */
		throw new Error( 'Name: string expected, got: ' + typeof name );

	// Allow chaining.
	let ret = { and: fn2 => expose( obj, fn2 ) };

	// In production, expose error functions.
	if ( env !== 'dev' ) { /* istanbul ignore if */
		fn = function() {
			throw new Error( 'Should not call test methods in production.' );
		};
	}

	// Create the test interface on the first exposure.
	if ( !obj.__test__ ) { /* instanbul ignore if */
		Object.defineProperty( obj, '__test__', {
			enumerable: false,
			configurable: false,
			writable: false,
			value: {}
		} );
	}

	obj.__test__[ name ] = fn;
	return ret;

}

function retrieve( obj, name ) {
	if ( typeof obj !== 'object' ) /* instanbul ignore if */
		throw new Error( 'Obj: object expected, got: ' + typeof name );
	if ( typeof name !== 'string' ) /* instanbul ignore if */
		throw new Error( 'Name: string expected, got: ' + typeof name );
	if ( !obj.__test__ )
		throw new Error( 'Object has no exposed test methods.' );
	if ( !obj.__test__[name] )
		throw new Error( 'Object has no exposed test method named ' + name + '.' );
	return obj.__test__[name];
}

module.exports = { expose, retrieve };
