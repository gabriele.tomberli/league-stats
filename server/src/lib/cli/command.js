
function ensureNotNull( prop, name ) {
	if ( prop === null || prop === undefined )
		throw new Error( `${ name } cannot be null.` );
	return prop;
}

function freezeProperty( obj, name, value ) {
	Object.defineProperty(obj, name, {
		value,
		configurable: false
	});
}

class Command {

	constructor( command, description, func ) {

		let me = this;
		me.command = ensureNotNull( command, 'command' );
		me.description = ensureNotNull( description, 'description' );
		ensureNotNull( func, 'func' );

		me.handler = function( yargs ) {
			// Handler is always sync, func can be async.
			freezeProperty( yargs, '_command', me );
			freezeProperty( yargs, '_resolve', global.Promise.resolve( func( yargs ) ) );
		}

	}

	builder( yargs ) {
		return yargs;
	}

}

module.exports = Command;