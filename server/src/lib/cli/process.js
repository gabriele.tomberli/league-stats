#!/usr/bin/env node
const yargs = globalRequire( 'yargs/yargs' )
	, Promise = global.Promise
	, Command = require( './command' )
	, { isNil, isNumber, isString, castArray, filter, reject } = globalRequire( 'lodash' )
	;

module.exports = async function process( args, commands = [], context = {} ) {

	if ( isNil( args ) )
		throw new Error( 'Undefined arguments.' );

	args = castArray( args );
	const invalidArgs = filter( args, a => {
		if ( isNumber(a) )
			return false;
		if ( !isString(a) )
			return true;
		if ( a.trim().match(/.*\s.*/) )
			return true;
		return false;
	} );
	if ( invalidArgs.length )
		throw new Error( 'Invalid argument found: ' + invalidArgs[0] );

	commands = castArray( commands );
	const invlidCommands = reject( commands, c => c instanceof Command );
	if ( invlidCommands.length )
		throw new Error( 'Invalid command found: ' + invlidCommands[ 0 ] );

	return new Promise( resolve => {

		const parser = yargs()
			.demandCommand( 1 )
			.help();
		commands.forEach(command => parser.command(command));

		try {
			parser.parse( args, context, async ( err, argv, output ) => {

				if ( err ) {
					// Command is not valid for yargs.
					return resolve( { resolved: false, error: err, output } );
				}

				if ( output ) {
					// Command is an internal Yargs command, eg: --help
					return resolve( { resolved: true, data: undefined, output, command: null } );
				}

				if ( !argv._command || !(argv._command instanceof Command)) {
					// Command is not a Command, throw.
					return resolve( { resolved: false, error: new Error( 'Invalid command' ), data: undefined, output: `Invalid command: ${ argv._[0] }`, command: undefined } );
				}

				let data;
				if ( argv._resolve ) {
					let promise = Promise.resolve( argv._resolve );
					data = await promise.catch( error => resolve( { resolved: false, command: argv._command, error, output: error.stack } ) );
				} else {
					data = undefined;
				}

				return resolve( { resolved: true, command: argv._command, data, argv } );

			})

		} catch ( err ) {
			// Command handler failed.
			return resolve( { resolved: false, error: err, output: err.stack } );
		}

	} );
}
