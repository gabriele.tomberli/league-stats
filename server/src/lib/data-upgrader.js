
const Bluebird = globalRequire( 'bluebird' )
	, lodash = globalRequire( 'lodash' )
	;

class Transition {

	/**
	 * An immutable object to store a transition from a version to another, associating it to the function that is responsible to upgrade the data.
	 * @param {int} from the version from where this transition applies
	 * @param {int} to the version to which this transition will bring it's data
	 * @param {function} fn the function to migrate the data from the starting to the ending version. The function will be called on data of the old version and should return data in the new version.
	 */
	constructor( from, to, fn ) {
		this.from = from;
		this.to = to;
		this.apply = fn;
		Object.freeze( this );
	}

}

class UnavailableUpgradeError extends Error {

	constructor( data, from, to ) {
		super( `No transition found to upgrade data from version ${ from } to version ${ to }.` );
		this.data = data;
		this.from = from;
		this.to = to;
	}

}

class EmptyUpgradeError extends Error {

	constructor( data, transition ) {
		super( `Transition from version ${ transition.from } to version ${ transition.to } resulted in empty data` );
		this.data = data;
		this.transition = transition;
	}

}

/**
 * Given a set of transitions, calculates the minimum path required to reach a given destination starting from a given point. Returns null if no path can be calculated.
 * @param {Transition[]} transitions the set of transitions
 * @param {int} from the starting point for the path
 * @param {int} to the required ending point for the path
 */
function getVersionTransitions( transitions, from, to, recursion = 0 ) {

	// console.debug( "#".repeat( recursion + 1 ), from, '->', to, 'Search started.' );
	let endingTransitions = lodash.filter( transitions, transition => transition.to === to );
	let bestSteps = null, bestCount = Number.MAX_SAFE_INTEGER;

	let exactTransition = lodash.find( endingTransitions, transition => transition.from === from );
	if ( exactTransition ) {
		// A transition exists from from to to. We have reached our destination, and no best path can be found.
		bestSteps = [ exactTransition ];
		bestCount = 1;
	} else {

		lodash.each( endingTransitions, transition => {
			// Recursively try to reach the starting point of this transition (so that steps + t => end).
			// The complete transition in this case would be: ( from -> transition.from ) -> transition.to
			// console.debug( "#".repeat( recursion + 1 ), from, '->', to, 'Exploring subpath: ' + from + ' -> ' + transition.from + ' -> ' + transition.to );
			let steps = getVersionTransitions( transitions, from, transition.from, recursion + 1 );
			if ( steps ) {
				// The first part has N steps in between, then with one more step the destination is reached.
				steps.push( transition );
				// Check if with this transition the whole path is shorter.
				if ( steps.length < bestCount ) {
					bestSteps = steps;
					bestCount = steps.length;
				}
			} else {
				// console.debug( "#".repeat( recursion + 1 ), from, '->', transition.from, 'No path found.' );
			}
		} );
	}

	/*
	if ( bestSteps ) {
		console.debug( "#".repeat( recursion + 1 ), from, '->', to, 'Path found: ', bestSteps.reduce( ( value, cur ) => value = value == null ? `${ cur.from } -> ${ cur.to }` : `${ value } -> ${ cur.to }`, null ) );
	} else {
		console.debug( "#".repeat( recursion + 1 ), from, '->', to, 'No path found.' );
	}
	*/

	return bestSteps;

}

class DataUpgrader {

	constructor() {
		this.lastVersion = -1;
		this.transitions = [];
	}

	createTransition( from, to, fn ) {
		if ( !lodash.isNumber( from ) || from < 0 )
			throw new Error( `Invalid argument from, positive number expected, got: ${ from }.` );
		if ( !lodash.isNumber( to ) || to < 0 )
			throw new Error( `Invalid argument to, positive number expected, got: ${ to }.` );
		if ( !lodash.isFunction( fn ) )
			throw new Error( `Invalid argument fn, function expected, got: ${ fn }.` );
		if ( to <= from )
			throw new Error( `Invalid argument to, expected a value greater than from (${ from }), got: ${ to }.` );
		if ( lodash.find( this.transitions, { from: from, to: to } ) )
			throw new Error( `Duplicated transition found from ${ from } to ${ to }.` );
		const transition = new Transition( from, to, fn );
		this.transitions.push( transition );
		this.lastVersion = Math.max( this.lastVersion, to );
		return transition;
	}

	/**
	 *
	 * @param {any} data the data to be upgraded
	 * @param {int} the required new version, or lastVersion if not given
	 */
	async upgrade( data, to = this.lastVersion ) {

		if ( !data )
			throw new Error( `Invalid argument data, object expected, got ${ data }.` );

		let from = data._version || 0;
		let dataCopy = lodash.cloneDeep( data );

		// console.info( `Requested to go from ${ from } to ${ to } for: ${ JSON.stringify( data, null, 2 ) }.` );
		if ( from >= to )
			return Bluebird.resolve( data );

		let transitions = getVersionTransitions( this.transitions, from, to );
		// console.info( `Found ${ transitions.length } to go from ${ from } to ${ to }.` );

		if ( !transitions )
			return Bluebird.reject( new UnavailableUpgradeError( data, from, to ) );

		return Bluebird.reduce( transitions, async ( current, transition ) => {
			// console.info( 'applying transition: ' + from + ' -> ' + transition.to );
			return Bluebird.resolve( transition.apply( current ) )
				.then( newData => {
					if ( !newData )
						throw new EmptyUpgradeError( current, transition );
					newData._version = transition.to;
					return newData;
				} )
		}, dataCopy );

	}

	interface() {
		return Object.freeze( {
			upgrade: this.upgrade.bind( this ),
			lastVersion: this.lastVersion
		} );
	}

}

module.exports = DataUpgrader;
module.exports.UnavailableUpgradeError = UnavailableUpgradeError;
module.exports.EmptyUpgradeError = EmptyUpgradeError;
