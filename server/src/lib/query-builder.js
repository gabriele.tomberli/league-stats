
const a = 1 // eslint-disable-line no-unused-vars
	, { assign, constant, isDate, isNumber, isString, identity, reduceRight } = globalRequire( 'lodash' )
	, datefns = globalRequire( 'date-fns' )
	;

function QueryBuilder( logEntity = false ) {
	return new InternalQueryBuilder( { logEntity, mainOperator: '$and', isArray: true } );
}

function InternalQueryBuilder( { logEntity, mainOperator, isArray } = { logEntity: false, mainOperator: '$and', isArray: true } ) {

	let me = this;

	// Core
	const operations = [];

	const toDate = param => {
		if ( isDate( param ) )
			return param;
		if ( isNumber( param ) )
			return new Date( param );
		if ( isString( param ) ) {
			let dt = datefns.parse( param, 'dd/MM/yyyy', 0 );
			if ( !datefns.isValid( dt ) ) throw new Error( 'Invalid date: ' + param );
			return dt;
		}
		throw new Error( 'Invalid date: ' + param );
	};

	const getUtcTime = dt => {
		const toUtcOffset = dt.getTimezoneOffset() * 60 * 1000;
		return new Date( dt.getTime() + toUtcOffset ).getTime();
	};

	const createFilterFunction = ( name, operator, ...transformations ) => {
		return function( value ) {

			// If value is undefined, no filter to add.
			if ( value === undefined )
				return me;

			// Transform value using the provided transformations, if any.
			transformations.push( identity );
			let v = reduceRight( transformations, ( current, transformation ) => transformation( current ), value );

			// Create the new operation
			let ret = { [ name ]: { [ operator ]: v } };
			let operation = constant( ret );

			// Add the new operation to the array
			operations.push( operation );

			return me;

		};
	};

	const chain = ( newMainOperator, newIsArray = true ) => {
		let builder = new InternalQueryBuilder( { logEntity: false, mainOperator: newMainOperator, isArray: newIsArray } );
		operations.push( builder.build );
		return builder;
	};

	me.build = () => {

		const operationsArray = operations.map( op => op.call( null ) );
		if ( operationsArray.length === 0 )
			return {};

		const operation = isArray ? operationsArray : operationsArray.reduce( assign, {} );
		let query = { [ mainOperator ]: operation };

		if ( logEntity )
			console.info( `Created query parameters for '${ logEntity }': ${ JSON.stringify( query ) }` );

		return query;
	};

	// Equality
	me.equals = ( name, value ) => createFilterFunction( name, '$eq' )( value );
	me.notEquals = ( name, value ) => createFilterFunction( name, '$neq' )( value );

	// Dates
	me.afterDate = ( name, value ) => createFilterFunction( name, '$gte', getUtcTime, datefns.startOfDay, toDate )( value );
	me.afterDateTime = ( name, value ) => createFilterFunction( name, '$gte', getUtcTime, toDate )( value );
	me.strictlyAfterDate = ( name, value ) => createFilterFunction( name, '$gt', getUtcTime, datefns.startOfDay, toDate )( value );
	me.strictlyAfterDateTime = ( name, value ) => createFilterFunction( name, '$gt', getUtcTime, toDate )( value );
	me.beforeDate = ( name, value ) => createFilterFunction( name, '$lte', getUtcTime, datefns.endOfDay, toDate )( value );
	me.beforeDateTime = ( name, value ) => createFilterFunction( name, '$lte', getUtcTime, toDate )( value );
	me.strictlyBeforeDate = ( name, value ) => createFilterFunction( name, '$lt', getUtcTime, datefns.endOfDay, toDate )( value );
	me.strictlyBeforeDateTime = ( name, value ) => createFilterFunction( name, '$lt', getUtcTime, toDate )( value );

	// Logic
	me.and = () => chain( '$and' );
	me.or = () => chain( '$or' );
	me.elemMatch = ( name ) => {
		let chainBuilder = new InternalQueryBuilder( { logEntity: false, mainOperator: '$elemMatch', isArray: false } );
		operations.push( () => ({ [name]: chainBuilder.build() }) );
		return chainBuilder;
	};

	return me;

}

module.exports = QueryBuilder;
