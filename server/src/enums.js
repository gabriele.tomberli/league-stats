
/* eslint-disable no-unused-vars */

const lodash = globalRequire( 'lodash' )
	;

let Enum = function( data ) {
	for ( let key in data ) {
		Object.defineProperty( this, key, {
			writable: false,
			readable: true,
			enumerable: true,
			configurable: false,
			value: data[ key ]
		} );
	}

	Object.defineProperty( this, 'UNDEFINED', {
		writable: false,
		readable: true,
		enumerable: false,
		configurable: false,
		value: { id: null, name: 'N/A', description: 'N/A', valid: false }
	} );

	this.getById = id => lodash.find( data, { id: id } ) || this.UNDEFINED;
};

let enums = {};

enums.Maps = new Enum( {
	SUMMONERS_RIFT: { name: 'Summoner\'s Rift', teams: 2, players: 5 },
	TWISTED_TREELINE: { name: 'Twisted Treeline', teams: 2, players: 3 },
	HOWLING_ABYSS: { name: 'Howling Abiss', teams: 2, players: 5 }
} );

enums.QueueTypes = new Enum( {
	RANKED_SOLO_SR: { id: "RANKED_SOLO_5x5", name: 'Ranked Solo', solo: true, team: false, flex: false, map: enums.Maps.SUMMONERS_RIFT },
	RANKED_FLEX_SR: { id: "RANKED_FLEX_SR", name: 'Ranked Flex 3v3', solo: false, team: false, flex: true, map: enums.Maps.SUMMONERS_RIFT },
	RANKED_FLEX_TT: { id: "RANKED_FLEX_TT", name: 'Ranked Flex 5v5', solo: false, team: false, flex: true, map: enums.Maps.TWISTED_TREELINE },

	RANKED_TFT: { id: "RANKED_TFT", name: 'Ranked Teamfight Tactics', solo: true, team: false, flex: false, map: enums.Maps.UNDEFINED },

	/** DEPRECATED */
	RANKED_TEAM_SR: { id: "RANKED_TEAM_5x5", name: 'Ranked Team 5x5', solo: false, team: true, flex: false, map: enums.Maps.SUMMONERS_RIFT },
	RANKED_TEAM_TT: { id: "RANKED_TEAM_3x3", name: 'Ranked Team 3x3', solo: false, team: true, flex: false, map: enums.Maps.TWISTED_TREELINE }
} );

enums.Tiers = new Enum( {
	UNRANKED: {
		id: 'UNRANKED', order: 1, name: 'Unranked'
	},
	BRONZE: {
		id: 'BRONZE', order: 2, name: 'Bronze'
	},
	SILVER: {
		id: 'SILVER', order: 3, name: 'Silver'
	},
	GOLD: {
		id: 'GOLD', order: 4, name: 'Gold'
	},
	PLATINUM: {
		id: 'PLATINUM', order: 5, name: 'Platinum'
	},
	DIAMOND: {
		id: 'DIAMOND', order: 6, name: 'Diamond'
	},
	MASTER: {
		id: 'MASTER', order: 7, name: 'Master'
	},
	CHALLENGER: {
		id: 'CHALLENGER', order: 8, name: 'Challenger'
	}
} );

enums.Ranks = new Enum( {
	ONE: {
		id: 'I', order: 5, name: 'I'
	},
	TWO: {
		id: 'II', order: 4, name: 'II'
	},
	THREE: {
		id: 'III', order: 3, name: 'III'
	},
	FOUR: {
		id: 'IV', order: 2, name: 'IV'
	},
	FIVE: {
		id: 'V', order: 1, name: 'V'
	}
} );

enums.Platforms = new Enum( {
	BR1:	{ region: 'BR',		endpoint: 'br1.api.riotgames.com', routingEndpoint: 'americas.api.riotgames.com' },
	EUN1:	{ region: 'EUNE',	endpoint: 'eun1.api.riotgames.com', routingEndpoint: 'europe.api.riotgames.com' },
	EUW1:	{ region: 'EUW',	endpoint: 'euw1.api.riotgames.com', routingEndpoint: 'europe.api.riotgames.com' },
	JP1:	{ region: 'JP',		endpoint: 'jp1.api.riotgames.com', routingEndpoint: 'asia.api.riotgames.com' },
	KR:		{ region: 'KR',		endpoint: 'kr.api.riotgames.com', routingEndpoint: 'asia.api.riotgames.com' },
	LA1:	{ region: 'LAN',	endpoint: 'la1.api.riotgames.com', routingEndpoint: 'americas.api.riotgames.com' },
	LA2:	{ region: 'LAS',	endpoint: 'la2.api.riotgames.com', routingEndpoint: 'americas.api.riotgames.com' },
	NA1:	{ region: 'NA',		endpoint: 'na1.api.riotgames.com', routingEndpoint: 'americas.api.riotgames.com' },
	NA:		{ region: 'NA',		endpoint: 'na1.api.riotgames.com', routingEndpoint: 'europe.api.riotgames.com' },
	OC1:	{ region: 'OCE',	endpoint: 'oc1.api.riotgames.com', routingEndpoint: 'americas.api.riotgames.com' },
	TR1:	{ region: 'TR',		endpoint: 'tr1.api.riotgames.com', routingEndpoint: 'europe.api.riotgames.com' },
	RU:		{ region: 'RU',		endpoint: 'ru.api.riotgames.com', routingEndpoint: 'europe.api.riotgames.com' },
	// PBE1:	{ region: 'PBE',	endpoint: 'pbe1.api.riotgames.com', routingEndpoint: 'europe.api.riotgames.com' }
} );
enums.Platforms.platform = platformId => enums.Platforms[ platformId.toUpperCase() ] || enums.Platforms.UNDEFINED;

module.exports = enums;
