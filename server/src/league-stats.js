
const RestService = globalRequire( 'src/core/rest-service.js' )
	, Database = globalRequire( 'src/core/database.js' )
	, Server = globalRequire( 'src/core/server.js' )
	, Configuration = globalRequire( 'src/lib/configuration.js' )

	, setCleanupFunction = globalRequire( 'node-cleanup' )
	, path = globalRequire( 'path' )
	, ML = globalRequire( '@zelgadis87/ml.js' )
	;

function LeagueStats( configuration ) {

	let me = this;

	// Interface

	me.activate = activate;
	me.deactivate = deactivate;
	me.resolve = resolve;

	// Implementation

	const config = configuration.data;

	function deactivate() {
		return ML.stop().return( me );
	}

	function activate() {
		return ML.start()
			.then( () => setCleanupFunction( ( exitCode, signal ) => deactivate() ) )
			.return( me );
	}

	function resolve( arg ) {
		return ML.resolve( arg );
	}

	ML.registerValue( 'configuration', configuration );
	ML.register( 'restService', [], () => new RestService( config.rest.baseUrl, config.rest.key ) );
	ML.register( 'database', [], () => new Database( config.database.url ).activate(), db => db.close() );
	ML.register( 'server', [], () => new Server(), s => s.deactivate() );
	ML.registerDirectory( path.join( __dirname, 'modules' ), true );

	return me;

}

module.exports = {
	init: async ( configurationFile, secretKey ) => {
		let config = await Configuration.init( configurationFile, secretKey );
		return new LeagueStats( config );
	}
};
