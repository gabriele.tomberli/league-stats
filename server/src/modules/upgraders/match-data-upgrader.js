
const DataUpgrader = globalRequire( 'src/lib/data-upgrader' )
	, lodash = globalRequire( 'lodash' )
	;

function calculateMatchParticipantsAndTeamsV1( match ) {
	match.participants = _calculateMatchParticipantsV1( match );
	match.teams = _calculateMatchTeamsV1( match );
	return match;
}

function calculateMatchParticipantsAndTeamsV2( match ) {
	match.participants = _calculateMatchParticipantsV2( match );
	match.teams = _calculateMatchTeamsV1( match );
	return match;
}

function _calculateMatchParticipantsV1( match ) {
	return lodash.map( match.participants, p => {
		let identity = lodash.find( match.participantIdentities, i => p.participantId === i.participantId );
		if ( identity ) p._player = identity.player;
		return p;
	} );
}

function _calculateMatchParticipantsV2( match ) {
	return lodash.map( match.participants, p => {
		let identity = lodash.find( match.participantIdentities, i => p.participantId === i.participantId );
		if ( identity ) {
			delete p._player; // For 4.1 data.
			p._summonerName = identity.player.summonerName;
			p._accountId = identity.player.currentAccountId;
		}
		return p;
	} );
}

function _calculateMatchTeamsV1( match ) {
	return lodash.map( match.teams, t => {
		let participants = lodash.filter( match.participants, p => p.teamId === t.teamId );
		if ( participants ) t._participants = participants;
		return t;
	} );
}

let matchDataUpgrader = new DataUpgrader();
matchDataUpgrader.createTransition( 3, 3.1, calculateMatchParticipantsAndTeamsV1 );
matchDataUpgrader.createTransition( 4, 4.1, calculateMatchParticipantsAndTeamsV1 );
matchDataUpgrader.createTransition( 3.1, 4.1, match => match );
matchDataUpgrader.createTransition( 4, 4.2, calculateMatchParticipantsAndTeamsV2 );
matchDataUpgrader.createTransition( 4.1, 4.2, calculateMatchParticipantsAndTeamsV2 );

module.exports = matchDataUpgrader.interface();
