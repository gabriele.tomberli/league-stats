
const a = 1 // eslint-disable-line no-unused-vars
	, Bluebird = globalRequire( 'bluebird' )
	, qs = globalRequire( 'qs' )
	, MatchQuery = globalRequire( 'src/lib/match-query' )
	;

function MatchesController( server, matchesStore, usersStore, championsStore ) {

	async function findMatches( querystring = '', { count } = { count: false } ) {

		const errors = [];

		const params = qs.parse( querystring, { allowDots: true } );

		const matchQuery = new MatchQuery();
		matchQuery.createdAfter( params.createdAfter );
		matchQuery.createdBefore( params.createdBefore );

		let players = [ params.player1, params.player2, params.player3, params.player4 ].filter( Boolean );
		await Bluebird.each( players, async player => {
			let p = {};

			if ( player.accountId ) {
				p.accountId = player.accountId;
			} else if ( player.name ) {
				let user = await usersStore.getByName( player.name );
				if ( !user ) {
					errors.push( 'No summoner found with name: ' + player.name );
					return;
				}
				p.accountId = user.accountId;
			}

			if ( player.championId ) {
				p.championId = parseInt( player.championId );
			} else if ( player.champion ) {
				let champion = await championsStore.getByName( player.champion );
				if ( !champion ) {
					errors.push( 'No champion found with name: ' + player.champion );
					return;
				}
				p.championId = champion._id;
			}

			matchQuery.player( p );
		} );

		if ( errors.length ) {
			return errors; // TODO: Signal error status code.
		}

		const query = matchQuery.build();
		if ( count )
			return matchesStore.count( query );

		let projection = {
			gameId: 1,
			platformId: 1,
			gameCreation: 1,
			gameDuration: 1,
			queueId: 1,
			mapId: 1,
			seasonId: 1,
			gameMode: 1,
			gameType: 1
		};

		return matchesStore.find( query, projection );

	}

	function activate() {
		server.router( '/matches' )
			.get( '/', async ctx => ctx.body = await findMatches( ctx.request.querystring ) )
			.get( '/count/', async ctx => ctx.body = await findMatches( ctx.request.querystring, { count: true } ) )
		;
	}

	activate();
	return this;

}

module.exports = MatchesController;
