
function HelloController( server ) {

	server.router( '/hello' )
		.get( '/', ctx => ctx.body = 'Hello world!' );

	return this;

}

module.exports = HelloController;