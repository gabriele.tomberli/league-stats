
const Bluebird = globalRequire( 'bluebird' )
	, lodash = globalRequire( 'lodash' )
	, moment = globalRequire( 'moment' )
	, test = globalRequire( 'src/lib/test' )
	, MatchNotFoundError = globalRequire( 'src/errors' ).MatchNotFoundError
	;

function MatchFetcher( matchesApi, matchesStore, usersStore, matchDataUpgrader ) {

	let me = this

	// Interface
	me.fetchAndStoreUserRecentGames = fetchAndStoreUserRecentGames;
	me.upgradeOutdatedData = upgradeOutdatedData;

	// Implementation

	async function fetchAndStoreUserRecentGames( user, forced = false ) {

		let now = new Date().getTime();
		let updateUser = () => {
			user._lastRecentGamesCheck = now;
			return usersStore.update( user._id, user );
		};

		let last = forced ? null : await getLastKnownMatch( user );

		let fetchPromise;
		if ( !last ) {
			console.info( `\nFetching ALL matches for user ${ user.name }...` );
			fetchPromise = matchesApi.all( user._platformId.toUpperCase(), user.puuid );
		} else {
			console.info( `\nFetching recent matches for user ${ user.name } (starting from ${ moment( last ).format( 'YYYY-MM-DD HH:mm' ) })...` );
			fetchPromise = matchesApi.recent( user._platformId.toUpperCase(), user.puuid, last );
		}

		return fetchPromise
			.tap( matches => console.info( `  Found ${ matches.length } match${ matches.length === 1 ? '' : 'es' }` ) )
			.then( matches => matches.reverse() )
			.each( ( match, i, n ) => registerMatch( user, match, i, n ) )
			.filter( match => !!match )
			.tap( updateUser );
	}

	async function registerMatch( user, matchId, i, n ) {

		let accountId = user.accountId
			, platformId = user._platformId
			;

		return retrieveMatchFromStore( matchId ).then( async data => {

			if ( !data ) {
				console.info( `    ${ percentage( i, n ) }Fetching complete data for match ${ matchId }...` );
				return registerNewMatch( platformId, matchId, accountId )
					.then( matchData => console.info( `      Registered match ${ matchData.gameId }.` ) )
					.catch( err => {
						if ( err instanceof MatchNotFoundError ) {
							console.warn( `      Could not register match ${ matchId }.` );
							return null;
						} else throw err;
					} );
			} else if ( !lodash.includes( data._accounts, accountId ) ) {
				console.info( `    ${ percentage( i, n ) }Match ${ matchId } already registered for other players. (${ await alreadyRegisteredSummoners( data._accounts ) })` );
				return updateMatch( data._id, { _accounts: [ ...data._accounts, accountId ] } );
			} else {
				console.info( `    ${ percentage( i, n ) }Match ${ matchId } already registered.` );
				return Bluebird.resolve( data );
			}

		} )
	}

	async function getLastKnownMatch( user ) {
		if ( user._lastRecentGamesCheck )
			return user._lastRecentGamesCheck;

		let lastMatches = await matchesStore.getLatestMatchForUsers( [ user ] );
		if ( lastMatches[ user.accountId ] )
			return lastMatches[ user.accountId ];

		return null;
	}

	async function fetchMatch( platformId, matchId, accountId ) {
		return matchesApi.match( platformId, matchId ).then( matchData => {
			matchData._accounts = [ accountId ];
			matchData._version = matchesApi.version;
			return matchData;
		} );
	}

	async function registerNewMatch( platformId, matchId, accountId ) {
		return fetchMatch( platformId, matchId, accountId )
			.then( upgradeMatchData )
			.then( insertMatch );
	}

	async function upgradeOutdatedData() {
		let promises = [];
		return matchesStore.getOutdatedDataCursor( matchDataUpgrader.lastVersion ).then( cursor => {
			return new Bluebird( ( resolve, reject ) => {
				cursor.forEach( data => promises.push( upgradeData( data ) ), err => {
					if (!err) {
						cursor.close();
						resolve();
					}
					reject(err);
				} );
			} )
		} ).then( () => Bluebird.all( promises ) );
	}

	async function upgradeData( match ) {
		console.info( 'Upgrading outdated match: ' + match._id );
		return matchesStore.update( match._id, await matchDataUpgrader.upgrade( match ) );
	}

	// Shortcut methods.

	async function retrieveMatchFromStore( matchId ) {
		return matchesStore.getByMatchId( matchId );
	}

	async function upgradeMatchData( matchData ) {
		return matchDataUpgrader.upgrade( matchData );
	}

	async function insertMatch( matchData ) {
		return matchesStore.insert( matchData );
	}

	async function updateMatch( matchId, change ) {
		return matchesStore.update( matchId, change );
	}

	let allUsers;
	async function getAllUsers() {
		if ( !allUsers )
			allUsers = lodash.keyBy( await usersStore.getAllUsers(), 'accountId' );
		return allUsers;
	}

	function percentage( i, n ) {
		return n >= 10 ? ( i / n * 100 ).toFixed( 2 ).toString().padStart( 6, ' ' ) + '%. ' : '';
	}

	async function alreadyRegisteredSummoners( summoners ) {
		let users = await getAllUsers();
		return lodash( summoners )
			.map( accountId => users[ accountId ] ? users[ accountId ].name : accountId )
			.join( ', ' );
	}

	test.expose( me, percentage )
		.and( alreadyRegisteredSummoners );

	return me

}

module.exports = MatchFetcher

