

const a = 1 // eslint-disable-line no-unused-vars
	, Bluebird = globalRequire( 'bluebird' )
	, lodash = globalRequire( 'lodash' )
	;

function SummonerFetcher( summonersApi, usersStore, matchesStore, positionsStore ) {

	let me = this;

	// Interface
	me.fetchSummoner = fetchSummoner;

	// Implementation

	async function fetchSummoner( user ) {

		if ( !user._version || user._version <= 3 )
			return _fetchSummonerV3( user );

		console.info( `\nFetching summoner ${ user.name }...` );
		return Bluebird.resolve( user )
			.then( async u => {
				let newData = await summonersApi.fetchSummonerByPuuid( u._platformId, u.puuid );
				return lodash.extend( {}, user, newData );
			} )
			.then( u => usersStore.update( u._id, u ) )
			.tap( u => console.info( '  ... ✔ Summoner updated' ) );

	}

	async function _fetchSummonerV3( user ) {
		// Handle legacy V3 user by searching by username instead of puuid.
		// Then deletes and reinserts the new user with the new _id.
		console.info( `\nFetching summoner ${ user.name }... (migrating from V3)` );
		return Bluebird.resolve( user )
			.then( u => summonersApi.fetchSummonerByName( u._platformId, u.name ) )
			.then( newData => lodash.extend( {}, user, newData ) )
			.then( async newSummoner => {
				newSummoner._version = 4;
				newSummoner._oldSummonerId = user._id;
				newSummoner._oldAccountId = user.accountId;
				await matchesStore.migrateFromV3ToV4( newSummoner._oldAccountId, newSummoner.accountId );
				console.info( '  ... ✔ Matches migrated to V4' );
				await positionsStore.migrateFromV3ToV4( newSummoner._oldSummonerId, newSummoner._id );
				console.info( '  ... ✔ Positions migrated to V4' );
				await usersStore.insertUser( newSummoner );
				await usersStore.deleteUser( newSummoner._oldSummonerId );
				console.info( '  ... ✔ Summoner migrated to V4' );
				return newSummoner;
			} );
	}

	return me;

}

module.exports = SummonerFetcher;

