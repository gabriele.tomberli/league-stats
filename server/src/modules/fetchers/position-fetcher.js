
const Bluebird = globalRequire( 'bluebird' )
	, lodash = globalRequire( 'lodash' )
	, Enums = globalRequire( 'src/enums' )
	;

const CURRENT_VERSION = 4
	, UNIQUE_FIELDS = [ 'season', 'platformId', 'summonerId', 'leagueId', 'wins', 'losses', 'leaguePoints', 'tier', 'rank', 'inactive', 'miniSeries' ]
	;

/**
 * FIXME: Riot APIs cannot be trusted to determine the current season.
 * For now, we will use a manually updated constant.
 * See https://gitlab.com/gabriele.tomberli/league-stats/issues/30 for details.
 */
const CURRENT_SEASON_ID = 13;

function PositionFetcher( leaguesApi, positionsStore, matchesStore ) {

	let me = this

	// Interface
	me.fetchUserPosition = fetchUserPosition;

	// Implementation
	async function fetchUserPosition( user ) {

		let platformId = user._platformId, summonerId = user._id;

		/*
		let lastMatch = await matchesStore.getLastMatch( platformId );
		if ( !lastMatch )
			throw new Error( `Unable to retrieve current season data for platform ${ platformId }` );
		let currentSeason = lastMatch.seasonId;
		*/
		let currentSeason = CURRENT_SEASON_ID;

		console.info( `\nFetching current League position for user ${ user.name } (Season ${ currentSeason })...` );
		return Bluebird.resolve( leaguesApi.getSummonerPositions( platformId, summonerId ) )
			.then( positionsData => {
				if ( !positionsData || !positionsData.length ) {
					console.info( `    No league found for user.` );
					return [];
				}
				return positionsData;
			} )
			.map( positionData => convertPositionDto( platformId, currentSeason, positionData ) )
			.each( position => {
				return positionsStore.getCurrentPosition( position.platformId, position.summonerId, position.queueType, position.season )
					.then( currentPosition => {

						let curQueueType = Enums.QueueTypes.getById( position.queueType ),
							curTier = Enums.Tiers.getById( position.tier ),
							curRank = Enums.Ranks.getById( position.rank ),
							curLeaguePoints = position.leaguePoints;

						if ( !currentPosition ) {
							let message = `    ${ curQueueType.name }: Entered ${ curTier.name } ${ curRank.name } (${ curLeaguePoints } LP)`;
							if ( position.miniSeries )
								message += ' ' + displayMiniSeries( position.miniSeries );
							console.info( message );
							return Bluebird.resolve( positionsStore.insertPosition( position ) ).return( true );
						} else if ( isEqualBy( position, currentPosition, UNIQUE_FIELDS ) ) {
							let message = `    ${ curQueueType.name }: Still in ${ curTier.name } ${ curRank.name } (${ curLeaguePoints } LP)`;
							if ( currentPosition.miniSeries )
								message += ' ' + displayMiniSeries( currentPosition.miniSeries );
							console.info( message );
							return Bluebird.resolve( positionsStore.updateLastSeen( currentPosition ) ).return( false );
						} else {

							let oldTier = Enums.Tiers.getById( currentPosition.tier ),
								oldRank = Enums.Ranks.getById( currentPosition.rank ),
								oldLeaguePoints = currentPosition.leaguePoints,
								order,
								sentences = [ 'Fallen to {{to}} from {{from}}', 'Still at {{from}}', 'Risen to {{to}} from {{from}}' ];

							if ( curTier.order > oldTier.order ) {
								order = 1
							} else if ( curTier.order < oldTier.order ) {
								order = -1;
							} else {
								if ( curRank.order > oldRank.order ) {
									order = 1
								} else if ( curRank.order < oldRank.order ) {
									order = -1;
								} else {
									if ( position.leaguePoints > currentPosition.leaguePoints ) {
										order = 1
									} else if ( position.leaguePoints < currentPosition.leaguePoints ) {
										order = -1;
									} else {
										order = 0;
									}
								}
							}

							let from = `${ oldTier.name } ${ oldRank.name } (${ oldLeaguePoints } LP)`
							let to = `${ curTier.name } ${ curRank.name } (${ curLeaguePoints } LP)`

							if ( currentPosition.miniSeries )
								from += ' ' + displayMiniSeries( currentPosition.miniSeries );
							if ( position.miniSeries )
								to += ' ' + displayMiniSeries( position.miniSeries );

							console.info( `    ${ curQueueType.name }: ${ sentences[ order + 1 ].replace( '{{from}}', from ).replace( '{{to}}', to ) }` );
							return Bluebird.resolve( positionsStore.insertPosition( position ) ).return( true );
						}
					} );
			} );
	}

	let convertPositionDto = ( platformId, currentSeason, positionData ) => lodash.extend( positionData, {
		platformId: platformId,
		summonerId: positionData.summonerId,
		season: currentSeason, // The data just retrieved are for the current season
		firstSeen: new Date(), // The date where this position was first retrieved for this user
		lastSeen: new Date(), // The date where this position was last retrieved for this user
		version: CURRENT_VERSION
	} );

	let isEqualBy = ( a, b, fields ) => {
		let equal = true;
		for ( let field of fields ) {
			if ( !lodash.isEqual( a[ field ], b[ field ] ) ) {
				equal = false;
				break;
			}
		}
		return equal;
	};

	let displayMiniSeries = ms =>
		`[${ ms.progress.replace( /W/g, '•' ).replace( /L/g, '○' ).replace( /N/g, '-' ) }]`;

	return me

}

module.exports = PositionFetcher
