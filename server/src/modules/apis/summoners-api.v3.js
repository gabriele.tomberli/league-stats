
const { UserNotFoundError } = globalRequire( 'src/errors' )

function SummonersApiV3( restService ) {

	let me = this;

	// Interface

	me.version = 3;
	me.fetchSummonerByName = fetchSummonerByName;
	me.fetchSummonerByAccountId = fetchSummonerByAccountId;
	me.fetchSummonerById = fetchSummonerById;

	// Implementation

	const endpointSummonerById = 'summoner/v3/summoners/<summonerId>';
	const endpointSummonerByAccountId = 'summoner/v3/summoners/by-account/<accountId>';
	const endpointSummonerByName = 'summoner/v3/summoners/by-name/<summonerName>';

	let parseUser = ( platformId, user ) => {
		user._id = user.id;
		user._platformId = platformId.toUpperCase();
		delete user.id;
		return user;
	};

	function fetchSummonerById( platformId, summonerId ) {
		return restService.request( platformId, endpointSummonerById.replace( '<summonerId>', summonerId ) )
			.then( response => response.data )
			.then( user => parseUser( platformId, user ) )
			.catch( { statusCode: 404 }, reason => { throw new UserNotFoundError(); } );
	}

	function fetchSummonerByAccountId( platformId, accountId ) {
		return restService.request( platformId, endpointSummonerByAccountId.replace( '<accountId>', accountId ) )
			.then( response => response.data )
			.then( user => parseUser( platformId, user ) )
			.catch( { statusCode: 404 }, reason => { throw new UserNotFoundError(); } );
	}

	function fetchSummonerByName( platformId, summonerName ) {
		return restService.request( platformId, endpointSummonerByName.replace( '<summonerName>', encodeURIComponent( summonerName ) ) )
			.then( response => response.data )
			.then( user => parseUser( platformId, user ) )
			.catch( { statusCode: 404 }, reason => { throw new UserNotFoundError(); } );
	}

	return me;

}

module.exports = SummonersApiV3;
