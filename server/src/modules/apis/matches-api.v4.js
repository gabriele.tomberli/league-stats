
const { NotImplementedError, MatchNotFoundError } = globalRequire( 'src/errors' )
	;

const MAX_INDEX_DIFF = 100,
	MAX_TIME_DIFF = 7 * 24 * 60 * 60 * 1000;

function MatchesApiV4( restService ) {

	let me = this;

	// Interface

	me.version = 4;
	me.recent = getRecent;
	me.all = getAllMatches;
	me.match = getMatch;
	me.matchTimelines = getMatchTimelines;

	// Implementation

	const endpointMatchList = 'match/v4/matchlists/by-account/<accountId>';
	const endpointMatchById = 'match/v4/matches/<matchId>';

	function doGetAllMatchesByIndex( platformId, accountId, beginIndex, endIndex = beginIndex + MAX_INDEX_DIFF ) {
		return doGetMatches( platformId, accountId, { beginIndex: beginIndex, endIndex: endIndex } )
			.then( data => {
				if ( data.totalGames > data.endIndex ) {
					return doGetAllMatchesByIndex( platformId, accountId, data.endIndex, data.endIndex + MAX_INDEX_DIFF ).then( matches => {
						return  data.matches.concat( matches );
					} );
				} else {
					return data.matches;
				}
			} )
			.catch( { statusCode: 404 }, reason => {
				// This is needed in case the player didn't play any matches in the given time period.
				if ( beginIndex === 0 ) {
					return [];
				} else throw reason;
			} );
	}

	function doGetAllMatchesByTime( platformId, accountId, beginTime, endTime = beginTime + MAX_TIME_DIFF ) {
		return doGetMatches( platformId, accountId, { beginTime: beginTime, endTime: endTime } )
			.then( data => {
				let now = new Date().getTime();
				if ( endTime < now ) {
					return doGetAllMatchesByTime( platformId, accountId, endTime, endTime + MAX_TIME_DIFF ).then( matches => {
						return matches.concat( data.matches );
					} );
				} else {
					return data.matches;
				}
			} )
			.catch( { statusCode: 404 }, reason => {
				// This is needed in case the player didn't play any matches in the given time period.
				let now = new Date().getTime();
				if ( endTime < now ) {
					return doGetAllMatchesByTime( platformId, accountId, endTime );
				} else {
					return [];
				}
			} );
	}

	function doGetMatches( platformId, accountId, params ) {
		return restService.request( platformId, endpointMatchList.replace( '<accountId>', accountId ), { params: params } )
			.then( response => response.data );
	}

	function getAllMatches( platformId, accountId ) {
		return doGetAllMatchesByIndex( platformId, accountId, 0 );
	}

	function getRecent( platformId, accountId, lastMatchDate ) {
		return doGetAllMatchesByTime( platformId, accountId, lastMatchDate + 1 );
	}

	function getMatch( platformId, matchId ) {
		return restService.request( platformId, endpointMatchById.replace( '<matchId>', matchId ) )
			.then( response => response.data )
			.catch( { statusCode: 404 }, reason => {
				throw new MatchNotFoundError();
			} );
	}

	function getMatchTimelines( platformId, matchId ) {
		throw new NotImplementedError();
	}

	return me;

}

module.exports = MatchesApiV4;
