
const { NotImplementedError, MatchNotFoundError } = globalRequire( 'src/errors' )
	;

const MAX_INDEX_DIFF = 100,
	MAX_TIME_DIFF = 7 * 24 * 60 * 60 * 1000; // 7 days

function MatchesApiV5( restService ) {

	let me = this;

	// Interface

	me.version = 5;
	me.all = getAllMatches;
	me.recent = getRecentMatches;
	me.match = getMatch;
	me.matchTimelines = getMatchTimelines;

	// Implementation

	const endpointMatchList = 'match/v' + me.version + '/matches/by-puuid/<puuid>/ids';
	const endpointMatchById = 'match/v' + me.version + '/matches/<matchId>';

	function doGetAllMatchesByTime( platformId, puuid, startTime, startIndex = 0 ) {
		const MAX_MATCHES = 100;
		return doGetMatches( platformId, puuid, { startTime, count: MAX_MATCHES, start: startIndex } )
			.then( async data => {
				if ( data.length === MAX_MATCHES ) {
					return doGetAllMatchesByTime( platformId, puuid, startTime, startIndex + MAX_MATCHES ).then( matches => {
						return matches.concat( data );
					} );
				} else {
					return data;
				}
			} )
	}

	function doGetMatches( platformId, puuid, params ) {
		return restService.request( platformId, endpointMatchList.replace( '<puuid>', puuid ), { params }, true )
			.then( response => response.data );
	}

	function getAllMatches( platformId, puuid ) {
		return doGetAllMatchesByTime( platformId, puuid, 0 );
	}

	function getRecentMatches( platformId, puuid, lastMatchDate ) {
		return doGetAllMatchesByTime( platformId, puuid, Math.floor( lastMatchDate / 1000 ) + 1 );
	}

	function getMatch( platformId, matchId ) {
		return restService.request( platformId, endpointMatchById.replace( '<matchId>', matchId ), undefined, true )
			.then( response => response.data )
			.then( data => {
				return {
					dataVersion: data.metadata.dataVersion,
					matchId: data.metadata.matchId,
					participantIds: data.metadata.participants,
					...data.info
				};
			} )
			.then( match => { match._version = me.version; return match; } )
			.catch( { statusCode: 404 }, reason => {
				throw new MatchNotFoundError();
			} );
	}

	function getMatchTimelines( platformId, matchId ) {
		throw new NotImplementedError();
	}

	return me;

}

module.exports = MatchesApiV5;
