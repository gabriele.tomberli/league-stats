

function LeaguesApiV3( restService ) {

	let me = this;

	// Interface

	me.version = 3;
	me.getSummonerPositions = getSummonerPositions;
	me.getMasterLeague = getMasterLeague;
	me.getChallengerLeague = getChallengerLeague;
	me.getLeague = getLeague;

	// Implementation

	const endpointLeagueById = 'league/v3/leagues/<leagueId>';
	const endpointMasterLeagueByQueue = 'league/v3/masterleagues/by-queue/<queueType>';
	const endpointChallengerLeagueByQueue = 'league/v3/challengerleagues/by-queue/<queueType>';
	const endpointPositionsBySummoner = 'league/v3/positions/by-summoner/<summonerId>'

	function getLeague( platformId, leagueId ) {
		return restService.request( platformId, endpointLeagueById.replace( '<leagueId>', leagueId ) ).then( response => response.data );
	}

	function getMasterLeague( platformId, queueType ) {
		return restService.request( platformId, endpointMasterLeagueByQueue.replace( '<queueType>', queueType ) ).then( response => response.data );
	}

	function getChallengerLeague( platformId, queueType ) {
		return restService.request( platformId, endpointChallengerLeagueByQueue.replace( '<queueType>', queueType ) ).then( response => response.data );
	}

	function getSummonerPositions( platformId, summonerId ) {
		return restService.request( platformId, endpointPositionsBySummoner.replace( '<summonerId>', summonerId ) ).then( response => response.data );
	}

	return me;

}

module.exports = LeaguesApiV3;