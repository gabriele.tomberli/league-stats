

function LeaguesApiV4( restService ) {

	let me = this;

	// Interface

	me.version = 4;
	me.getSummonerPositions = getSummonerPositions;
	me.getMasterLeagues = getMasterLeagues;
	me.getGrandMasterLeagues = getGrandMasterLeagues;
	me.getChallengerLeagues = getChallengerLeagues;
	me.getLeague = getLeague;

	// Implementation

	const endpointLeagueById = 'league/v4/leagues/<leagueId>';
	const endpointMasterLeaguesByQueue = 'league/v4/masterleagues/by-queue/<queueType>';
	const endpointGrandMasterLeaguesByQueue = 'league/v4/challengerleagues/by-queue/<queueType>';
	const endpointChallengerLeaguesByQueue = 'league/v4/challengerleagues/by-queue/<queueType>';
	const endpointPositionsBySummoner = 'league/v4/entries/by-summoner/<summonerId>'

	function getLeague( platformId, leagueId ) {
		return restService.request( platformId, endpointLeagueById.replace( '<leagueId>', leagueId ) ).then( response => response.data );
	}

	function getMasterLeagues( platformId, queueType ) {
		return restService.request( platformId, endpointMasterLeaguesByQueue.replace( '<queueType>', queueType ) ).then( response => response.data );
	}

	function getGrandMasterLeagues( platformId, queueType ) {
		return restService.request( platformId, endpointGrandMasterLeaguesByQueue.replace( '<queueType>', queueType ) ).then( response => response.data );
	}

	function getChallengerLeagues( platformId, queueType ) {
		return restService.request( platformId, endpointChallengerLeaguesByQueue.replace( '<queueType>', queueType ) ).then( response => response.data );
	}

	function getSummonerPositions( platformId, summonerId ) {
		return restService.request( platformId, endpointPositionsBySummoner.replace( '<summonerId>', summonerId ) ).then( response => response.data );
	}

	return me;

}

module.exports = LeaguesApiV4;