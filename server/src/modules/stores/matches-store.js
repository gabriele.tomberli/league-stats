
const _ = globalRequire( 'lodash' )
	;

function MatchesStore( database ) {

	let me = this;

	// Interface
	me.getLatestMatchForUsers = getLatestMatchForUsers;

	me.get = get;
	me.getByMatchId = getByMatchId;
	me.insert = insert;
	me.update = update;
	me.find = find;
	me.count = count;
	me.aggregate = aggregate;
	me.getLastMatch = getLastMatch;
	me.migrateFromV3ToV4 = migrateFromV3ToV4;
	me.getOutdatedDataCursor = getOutdatedDataCursor;

	// Implementation

	let matches = database.create( 'matches', null, { castIds: false } );

	matches.createIndex( { 'participants._accountId': 1 } );
	matches.createIndex( { 'platformId': 1 } );
	matches.createIndex( { 'gameCreation': -1 } );

	let toId = ( platformId, gameId ) => platformId + '_' + gameId;

	/** @deprecated */
	function get( platformId, gameId ) {
		return matches.findOne( { _id: toId( platformId, gameId ) } )
	}

	function getByMatchId( matchId ) {
		return matches.findOne( { _id: matchId } );
	}

	function update( matchId, newData ) {
		return matches.update( { _id: matchId }, { $set: newData } ).then( _.constant( newData ) );
	}

	function insert( matchData ) {
		matchData._id = toId( matchData.platformId, matchData.gameId );
		return matches.insert( matchData ).then( _.constant( matchData ) );
	}

	function find( filters, projection ) {
		return matches.find( filters, { projection } );
	}

	function aggregate( pipeline ) {
		return matches.aggregate( pipeline );
	}

	function count( filters ) {
		return matches.count( filters );
	}

	/**
	 * Given a list of users, returns for each of them the latest game played known by the system.
	 * @param {User[]} users
	 * @returns Returns an object, whose keys are user ids and values are the timestamp of the latest game played. Users given in input who have not played any game will not be returned.
	 */
	function getLatestMatchForUsers( users ) {
		let ids = _.map( users, 'accountId' );
		let pipeline = [
			{ $match: { '_accounts': { $in: ids } } },
			{ $unwind: '$_accounts' },
			{ $match: { '_accounts': { $in: ids } } },
			{ $group: { _id: '$_accounts', last: { $max: '$gameCreation' } } }
		];
		return aggregate( pipeline ).then( arr => arr.reduce( ( ret, x ) => { ret[ x._id ] = x.last; return ret; }, {} ) );
	}

	function getLastMatch( platformId ) {
		return matches.findOne( { platformId: platformId }, { sort: { gameCreation: -1 }, limit: 1 } );
	}

	async function getOutdatedDataCursor( currentVersion ) {
		return find( { _version: { $lt: currentVersion } }, { rawCursor: true } );
	}

	async function migrateFromV3ToV4( oldAccountId, newAccountId ) {
		return matches.bulkWrite( [
			{
				updateMany: {
					filter: { _accounts: { $in: [ oldAccountId ] } },
					update: { $push: { _accounts: newAccountId } }
				}
			},
			{
				updateMany: {
					filter: { _accounts: { $in: [ oldAccountId ] } },
					update: { $pull: { _accounts: oldAccountId } }
				}
			}
		] );
	}

	return me;

}

module.exports = MatchesStore;
