
function ChampionsStore( database ) {

	let me = this;

	// Interface
	me.get = get;
	me.find = find;
	me.count = count;
	me.aggregate = aggregate;

	me.getByName = getByName;

	// Implementation

	// Database and index creation
	let champions = database.create( 'anag_champions', null, { castIds: false } );
	champions.createIndex( { 'name': 1 } );

	// Standard functionalities.
	function get( id ) {
		return champions.findOne( { _id: id } );
	}

	function aggregate( pipeline ) {
		return champions.aggregate( pipeline );
	}

	function count( filters ) {
		return champions.count( filters );
	}

	function find( filters, projection ) {
		return champions.find( filters, { projection } );
	}

	// Custom functionalities

	function getByName( name ) {
		return champions.findOne( { name } );
	}

	return me;

}

module.exports = ChampionsStore;