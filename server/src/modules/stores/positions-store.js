
const lodash = globalRequire( 'lodash' )
	, { NotImplementedError } = globalRequire( 'src/errors' )
	;

function PositionsStore( database ) {

	let me = this;

	// Interface
	me.getCurrentPosition = getCurrentPosition;
	me.insertPosition = insertPosition;
	me.updateLastSeen = updateLastSeen;
	me.getPositions = getPositions;
	me.migrateFromV3ToV4 = migrateFromV3ToV4;

	// Implementation

	let positions = database.create( 'positions', null, { castIds: false } );

	async function getCurrentPosition( platformId, summonerId, queueType, season ) {
		return positions.find( { platformId: platformId, summonerId: summonerId, queueType: queueType, season: season }, { rawCursor: true } )
			.then( cursor => cursor.sort( { lastSeen: -1 } ).limit( 1 ).toArray() )
			.then( lodash.head );
	}

	async function insertPosition( position ) {
		return positions.insert( position );
	}

	async function getPositions( platformId, queueType, opts ) {
		throw new NotImplementedError();
	}

	async function updateLastSeen( position ) {
		return positions.update( { _id: position._id }, { $set: { lastSeen: new Date() } } );
	}

	async function migrateFromV3ToV4( oldSummonerId, newSummonerId ) {
		return positions.update(
			{ accountId: `${ oldSummonerId }` },
			{
				$unset: {
					accountId: true,
					playerOrTeamId: true
				},
				$set: {
					summonerId: newSummonerId,
					position: 'NONE',
					version: 4
				},
				$rename: {
					playerOrTeamName: 'summonerName'
				}
			},
			{ multi: true }
		);
	}

	return me;

}

module.exports = PositionsStore;
