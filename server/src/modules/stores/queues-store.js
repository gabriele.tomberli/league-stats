
function QueuesStore( database ) {

	let me = this;

	// Interface
	me.getAllQueues = getAllQueues;

	// Implementation
	let queues = database.get( 'anag_queues', { castIds: false } );

	function getAllQueues() {
		return queues.find();
	}

	return me;

}

module.exports = QueuesStore;