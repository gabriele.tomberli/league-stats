
function UsersStore( database ) {

	let me = this;

	// Interface
	me.getAllUsers = getAllUsers;
	me.getByName = getByName;
	me.insertUser = insertUser;
	me.update = update;
	me.deleteUser = deleteUser;

	// Implementation
	let users = database.create( 'users', null, { castIds: false } );

	async function getAllUsers( filters ) {
		return users.find( filters );
	}

	async function insertUser( user ) {
		return users.insert( user );
	}

	async function getByName( name ) {
		let caseInsensitiveRegEx = new RegExp( [ '^', name, '$' ].join( '' ), 'i' );
		return users.find( { name: caseInsensitiveRegEx } ).then( usersFound => usersFound[ 0 ] );
	}

	async function update( userId, data ) {
		return users.update( { _id: userId }, { $set: data } ).then( () => data );
	}

	async function deleteUser( userId ) {
		return users.remove( { _id: userId } );
	}

	return me;

}

module.exports = UsersStore;
