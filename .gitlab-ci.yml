###
image: node:alpine

include:
  - local: shared/.gitlab-ci.shared.yml

.if-default-branch: &if-default-branch
  if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'

.if-merge-request: &if-merge-request
  if: $CI_MERGE_REQUEST_IID

.if-default-refs: &if-default-refs
  if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_MERGE_REQUEST_IID'

.if-detached-job: &if-detached-job
  if: $DETACHED_JOB

stages:
  - sync
  - submodules
  - analytics
  - triage

sync:
  extends:
    - .global-default
  stage: sync
  interruptible: false
  image: alpine
  variables:
    GIT_STRATEGY: none
  script:
    - echo "Ensure pipeline doesn't get interrupted!"
  rules:
    - <<: *if-detached-job
    - <<: *if-default-branch
    - <<: *if-default-refs
      when: manual
      allow_failure: true

server:
  stage: submodules
  trigger:
    include: server/.gitlab-ci.server.yml
    strategy: depend
  rules:
    - <<: *if-detached-job
      when: never
    - <<: *if-default-branch
    - <<: *if-default-refs
      changes:
        - "server/**/*"

client:
  stage: submodules
  trigger:
    include: client/.gitlab-ci.client.yml
    strategy: depend
  rules:
    - <<: *if-detached-job
      when: never
    - <<: *if-default-branch
    - <<: *if-default-refs
      changes:
        - "client/**/*"

sonar:
  extends:
    - .global-default
  stage: analytics
  image: ${CI_REGISTRY}/gabriele.tomberli/league-stats/sonarqube-scanner
  script:
    - cp ./ext/Sonar/sonar.properties ./sonar-project.properties
    - sonar-scanner -Dsonar.login=${SONAR_LOGIN} -Dsonar.branch.name=${CI_COMMIT_REF_NAME} -Dsonar.sources=./server/src,./client/src -Dsonar.javascript.lcov.reportPaths=./server/coverage/lcov.info,./client/coverage/lcov.info
  allow_failure: true
  rules:
    - <<: *if-detached-job
      when: never
    - <<: *if-default-branch
    - <<: *if-default-refs
      changes:
        - "server/**"
        - "client/**"

triage-dry-run:
  extends:
    - .global-default
  stage: triage
  image: ruby:alpine
  script:
    - gem install gitlab-triage
    - gitlab-triage --dry-run --token $TRIAGE_TOKEN --source-id $CI_PROJECT_PATH
  rules:
    - <<: *if-detached-job
      when: never
    - <<: *if-default-refs
      changes:
        - ".triage-policies.yml"
